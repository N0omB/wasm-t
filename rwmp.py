# -*- coding: iso-8859-15 -*-
import os, errno, re, io, shutil, subprocess, time, sys
import cfg, cpp_parser


def create_directory(directory):
    if not os.path.exists(directory):
        try:
            os.makedirs(directory)
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise


def init_file_dict():
    files_path = cfg.FILES_PATH
    files_list = [f for f in os.listdir(files_path) if os.path.isfile(os.path.join(files_path, f))]
    files_dict = {file_name: [] for file_name in files_list}
    for key in files_dict:
        file = io.open(files_path + "\\" + key, mode="r", encoding="iso-8859-15")
        # file = open(files_path + "\\" + key, "r", encoding="latin1")
        files_dict[key] = file.readlines()
        file.close()
    return files_dict


def load_intermediate_file_dict(path):
    files_list = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    files_dict = {file_name: [] for file_name in files_list}
    for key in files_dict:
        file = io.open(path + key, mode="r", encoding="iso-8859-15")
        files_dict[key] = file.readlines()
        file.close()
    return files_dict


def save_file_dict(files_dict, path):
    create_directory(path)
    for key, value in files_dict.items():
        file = io.open(path + key, mode="w+", encoding="iso-8859-15")
        # file = open(files_path + key, "w+", encoding="latin1")
        file.writelines(value)
        file.close()


def add_includes_to_file(file_content, includings, headers):
    line_last_include = includings[-1].line - 1
    for header in headers:
        if not any(header[0] == incl.name for incl in includings):
            file_content.insert(line_last_include, '#include ' + header[1] + header[0] + header[2] + '\n')
            line_last_include = line_last_include + 1
            if header[0] == "json.hpp":
                file_content.insert(line_last_include, '\n')
                file_content.insert(line_last_include, 'using json = nlohmann::json;\n')
                file_content.insert(line_last_include, '\n')
                line_last_include = line_last_include + 3
    return file_content


def delete_lines_in_file(file_content, start, end):
    return file_content[:start - 1] + file_content[end:]


def delete_unknown_includings(includings, files, main_file_name):
    for including in includings:
        basename = os.path.basename(os.path.splitext(including.name)[0])
        if (any(basename in name for name in cfg.MAIN_FILES) and basename not in main_file_name) or \
                not (including.name in cfg.STANDARD_LIBS or including.name in related_files_set):
            files[including.location] = delete_lines_in_file(files[including.location], including.line,
                                                             including.line)
    return files


def extract_message_map_info(file_content, idx, line):
    key = re.search(r"(?<=\()(?P<class>.*?)(?=,)", line).group('class')
    end = idx + 1
    content = []
    while not line.startswith("END_MESSAGE_MAP"):
        idx = idx + 1
        line = file_content[idx]
        if line.startswith("END_MESSAGE_MAP"):
            end = idx + 1
            break
        elif not line.strip().startswith("//"):
            content.append(re.sub(r'&.*::', '',line.strip()))
    message_map = {key: content}
    return message_map, end


def delete_param(param, main_cursor, files):
    param_locations = cpp_parser.get_param_locations(main_cursor, param)
    for par in param_locations:
        line = files[par.location][par.line-1]
        temp = line.split("(",1)[1].rsplit(")",1)[0].split(",")
        temp_array = []
        for p in temp:
            if param.name not in p:
                temp_array.append(p)
        line = line.split("(",1)[0] + "(" + ",".join(temp_array) + ")" + line.rsplit(")",1)[1]
        files[par.location][par.line - 1] = line
    return files


def delete_unused_params(cursor, files):
    params = cpp_parser.get_params(cursor, True)
    for param in params:
        function_node = param.node.lexical_parent
        function_file = os.path.basename(function_node.location.file.name)
        function_start = function_node.extent.start.line
        function_end = function_node.extent.end.line
        function_body = files[function_file][function_start + 1:function_end - 1]
        found_param_usage = False
        for line in function_body:
            if re.search(r"[^0-9a-zA-Z]" + param.name + "[^0-9a-zA-Z]", line):
                found_param_usage = True
        if not found_param_usage:
            files = delete_param(param, cursor, files)
    return files


def get_dialog_item_name_in_str(str):
    idc_start = str.split("ID", 1)[1]
    idc = "ID"
    while idc_start[0].isalnum() or idc_start[0] == '_':
        idc = idc + idc_start[0]
        idc_start = idc_start[1:]
    return idc


def set_EMSCRIPTEN_KEEPALIVE(str, name):
    idx = None
    temp_array = str.split(" ")
    for s in temp_array:
        if name in s:
            idx = temp_array.index(s)
    if idx is not None:
        temp_array.insert(idx, "EMSCRIPTEN_KEEPALIVE")
        str = " ".join(temp_array)
    return str


def minor_changes(files):
    message_maps = {}
    for file_name, file_content in files.items():
        delete_lines = []
        for idx, line in enumerate(file_content):
            tmp_line = line
            if line.startswith("extern ") or line.strip().startswith(": ") or line.strip().startswith("CDialog::") or \
                    "GetUXTheme(" in line or "EnableThemeDialogTexture(" in line \
                    or re.search(r"(^|\s)RTrim\(.*\);", line) or line.strip().startswith("DECLARE_MESSAGE_MAP()") \
                    or ".SetMask(" in line or re.sub("\s", "", line).startswith("enum{IDD=") \
                    or "DECLARE_DYNAMIC(" in line or "IMPLEMENT_DYNAMIC(" in line or line.strip().startswith("_cprintf(") \
                    or "SendMessage(" in line or "DestroyWindow(" in line:
                delete_lines.append((idx + 1, idx + 1))
                continue
            elif line.startswith("BEGIN_MESSAGE_MAP"):
                update_map, end = extract_message_map_info(file_content, idx, line)
                message_maps.update(update_map)
                delete_lines.append((idx + 1, end))
                continue
            elif line.strip().startswith("::SetFocus("):
                idc = get_dialog_item_name_in_str(line)
                tmp_line = re.match(r"^(\s)+\S", line).group(0)[:-1] + '::SetFocus("' + idc + '");\n'
            while re.search(r"::GetFocus\(\).=", tmp_line):
                tmp_str = tmp_line.split("::GetFocus()", 1)[1]
                non = "!" if tmp_str[0] == "!" else ""
                idc = get_dialog_item_name_in_str(tmp_str)
                tmp_line = tmp_line[:tmp_line.index("::GetFocus()")] + non + '::CheckFocus("' + idc + '"' + tmp_line[tmp_line.index(idc) + len(idc):]
            # Empty(.*) -> .*.empty()
            while "Empty(" in tmp_line:
                tmp_str = tmp_line.split("Empty(", 1)[1]
                open_parentheses = 1
                inner = ""
                for c in tmp_str:
                    if c == '(':
                        open_parentheses = open_parentheses + 1
                    elif c == ')':
                        open_parentheses = open_parentheses - 1
                    if open_parentheses == 0:
                        break
                    inner = inner + c
                tmp_line = tmp_line.split("Empty(", 1)[0] + inner + ".empty(" + tmp_str.split(inner, 1)[1]
            # BOOL -> bool
            tmp_line = re.sub(r"([^0-9a-zA-Z]*)BOOL(\s)", r"\1bool\2", tmp_line)
            # CString -> std::string
            tmp_line = re.sub(r"([^0-9a-zA-Z]*)CString(\s)", r"\1std::string\2", tmp_line)
            # CDateEdit -> std::string
            tmp_line = re.sub(r"([^0-9a-zA-Z]*)CDateEdit(\s)", r"\1std::string\2", tmp_line)
            # delete .GetData()
            tmp_line = re.sub(r"\.GetData\(\)", "", tmp_line)
            # TRUE -> true
            tmp_line = re.sub(r"([^0-9a-zA-Z])TRUE([^0-9a-zA-Z])", r"\1true\2", tmp_line)
            # FALSE -> false
            tmp_line = re.sub(r"([^0-9a-zA-Z])FALSE([^0-9a-zA-Z])", r"\1false\2", tmp_line)
            # deletion of :.*Dialog.*
            tmp_line = re.sub(r":.*CDialog.*$", "", tmp_line)
            # deletion of afx_msg
            tmp_line = re.sub(r"([^0-9a-zA-Z]*)afx_msg(\s)", r"\1", tmp_line)
            # deletion of ,MB_OK
            tmp_line = re.sub(r",MB_OK\)", ")", tmp_line)
            # _T(.*) -> std::string(.*)
            tmp_line = re.sub(r"([^0-9a-zA-Z])_T\(", r"\1std::string(", tmp_line)
            # return CDialog::PreTranslateMessage(pMsg); -> return true;
            tmp_line = re.sub( "(return )CDialog::PreTranslateMessage\(pMsg\)(;)", r"\1true\2", tmp_line)
            # WM_KEYDOWN -> "WM_KEYDOWN"
            tmp_line = re.sub(r"(WM_KEYDOWN)", r'"\1"', tmp_line)
            # VK_RETURN -> "VK_RETURN"
            tmp_line = re.sub(r"(VK_RETURN)", r'"\1"', tmp_line)
            # VK_TAB -> "VK_TAB"
            tmp_line = re.sub(r"(VK_TAB)", r'"\1"', tmp_line)
            # VK_DOWN -> "VK_DOWN"
            tmp_line = re.sub(r"(VK_DOWN)", r'"\1"', tmp_line)
            # VK_UP -> "VK_UP"
            tmp_line = re.sub(r"(VK_UP)", r'"\1"', tmp_line)
            # VK_F1 -> "VK_F1"
            tmp_line = re.sub(r"(VK_F1)", r'"\1"', tmp_line)
            # MessageBox( -> ::MessageBox(
            tmp_line = re.sub(r"(MessageBox\()", r'::\1', tmp_line)
            # NextDlgCtrl( -> ::NextDlgCtrl(
            tmp_line = re.sub(r"(NextDlgCtrl\()", r'::\1', tmp_line)
            # PrevDlgCtrl( -> ::PrevDlgCtrl(
            tmp_line = re.sub(r"(PrevDlgCtrl\()", r'::\1', tmp_line)
            # UpdateData -> "DoDataExchange"
            tmp_line = re.sub(r"UpdateData\(\)", "DoDataExchange(true)", tmp_line)
            tmp_line = re.sub(r"UpdateData\(([0-9a-zA-Z_]+)\)", r"DoDataExchange(\1)", tmp_line)
            # CTime::GetCurrentTime().GetDay() -> ::GetDay_RWMP()
            tmp_line = re.sub(r"CTime::GetCurrentTime\(\)\.GetDay\(\)", '::GetDay_RWMP()', tmp_line)
            # CTime::GetCurrentTime().GetMonth() -> ::GetMonth_RWMP()
            tmp_line = re.sub(r"CTime::GetCurrentTime\(\)\.GetMonth\(\)", '::GetMonth_RWMP()', tmp_line)
            # CTime::GetCurrentTime().GetYear() -> ::GetYear_RWMP()
            tmp_line = re.sub(r"CTime::GetCurrentTime\(\)\.GetYear\(\)", '::GetYear_RWMP()', tmp_line)
            # _ttoi( -> std::stoi(
            tmp_line = re.sub(r"([^0-9a-zA-Z_])_ttoi\(", r'\1std::stoi(', tmp_line)
            # .Format( exchange for std::string instead of Cstring
            if ".Format(" in tmp_line:
                pre_tmp, post_tmp = tmp_line.split(".Format(", 1)
                post_tmp = post_tmp.rsplit(")", 1)[0]
                pre_tmp = pre_tmp + " = "
                parants = 0
                i = 0
                for char in post_tmp:
                    if char == "(":
                        parants = parants + 1
                    elif char == ")":
                        parants = parants - 1
                    elif char == "," and parants == 0:
                        break
                    i = i + 1
                first = post_tmp[:i]
                scnd = post_tmp[i+1:].split(",")
                match = re.search(r'([^\"])%d([^\"])|([^\"])%d\"|\"%d([^\"])', first)
                while match is not None:
                    if first[match.start()] == '"':
                        first = first[:match.end() - 1] + '") + std::string("' + first[match.end() - 1:]
                    elif first[match.end()-1] == '"':
                        first = first[:match.start() + 1] + '") + std::string("' + first[match.start() + 1:]
                    else:
                        first = first[:match.start() + 1] + '") + std::string("%d") + std::string("' + first[match.end() - 1:]
                    match = re.search(r'([^\"])%d([^\"])|([^\"])%d\"|\"%d([^\"])', first)
                j = 0
                while 'std::string("%d")' in first:
                    index = first.index('std::string("%d")')
                    first = first[:index] + "std::to_string(" + scnd[j].strip() + ")" + first[index + 17:]
                    j = j + 1
                tmp_line = pre_tmp + first + ";\n"
            file_content[idx] = tmp_line
        delete_lines.sort(key=lambda t: t[0], reverse=True)
        for tup in delete_lines:
            file_content = delete_lines_in_file(file_content, tup[0], tup[1])
        files[file_name] = file_content
    return files, message_maps


def add_params_to_class_declaration(func, clas, files):
    cl = cpp_parser.Class(clas)
    lines = files[cl.location][cl.start - 1:cl.end - 1]
    for idx, line in enumerate(lines):
        if func.spelling in line:
            params = ""
            for param in func.parameters:
                params = params + param.type + " " + param.name + ", "
            params = params[:-2]
            line = line.split("(", 1)[0] + "(" + params + ")" + line.rsplit(")", 1)[1]
            lines[idx] = line
    files[cl.location] = files[cl.location][:cl.start - 1] + lines + files[cl.location][cl.end - 1:]
    return files


def edit_DoDataExchange(cursor, files, addings, main_name):
    do_data = cpp_parser.get_function_definition_by_name_in_mainFile(cursor, "DoDataExchange")
    if do_data is not None:
        # delete Parameter, not needed anymore, no need of method delete_param, due to overwriting params with add
        do_data.parameters = []
        # add new one
        do_data_header = files[main_name][do_data.start - 1]
        do_data_header = do_data_header.split("(", 1)[0] + "(bool direction)" + do_data_header.rsplit(")", 1)[1]
        files[main_name] = files[main_name][:do_data.start - 1] + [do_data_header] + files[main_name][do_data.start:]
        do_data.add_parameter("direction", "bool", do_data.location)
        # add new parameter to declaration in class
        files = add_params_to_class_declaration(do_data, do_data.node.semantic_parent, files)
        # reimplement body
        old_body = files[main_name][do_data.start + 1:do_data.end - 1]
        match = re.match(r"^(\s)+\S", files[main_name][do_data.start])
        tabs = ""
        if match is not None:
            tabs = match.group(0)[:-1]
        tabs = tabs + "\t"
        string_exchange = []
        for line in old_body:
            if line.strip().startswith("DDX_Text("):
                exchange = line.split("DDX_Text(pDX,", 1)[1].rsplit(")", 1)[0].strip().split(", ")
                string_exchange.append(exchange)
            elif line.strip().startswith("DDX_DateEditControl("):
                rc_part, data_part = line.split("DDX_DateEditControl(pDX,", 1)[1].rsplit(")", 1)[0].strip().split(", ")
                string_exchange.append((rc_part, data_part))
        new_body = [tabs + "if(direction)\n", tabs + "{\n", tabs + "}\n", tabs + "else\n", tabs + "{\n", tabs + "}\n"]
        tabs = tabs + "\t"
        front = []
        back = []
        char_index = 0
        for ex in string_exchange:
            rc_part, data_part = ex
            front_part = []
            front_part.append(tabs + data_part + " = (char*)EM_ASM_INT({\n")
            front_part.append(tabs + '\tvar return_string = get_string_of_element("' + rc_part + '");\n')
            front_part.append(tabs + '\tvar lengthBytes = lengthBytesUTF8(return_string)+1;\n')
            front_part.append(tabs + '\tvar stringOnWasmHeap = _malloc(lengthBytes);\n')
            front_part.append(tabs + '\tstringToUTF8(return_string, stringOnWasmHeap, lengthBytes+1);\n')
            front_part.append(tabs + '\treturn stringOnWasmHeap;\n')
            front_part.append(tabs + "},0);\n")
            front_part.append(tabs + "\n")
            back_part = []
            back_part.append(tabs + "char *cha" + str(char_index) + " = new char[" + data_part + ".size() + 1];\n")
            back_part.append(tabs + "strcpy(cha" + str(char_index) + ", " + data_part + ".c_str());\n")
            back_part.append(tabs + "EM_ASM_({\n")
            back_part.append(tabs + "\tvar data_string = UTF8ToString($0);\n")
            back_part.append(tabs + '\tset_string_of_element("' + rc_part + '", data_string);\n')
            back_part.append(tabs + "}, cha" + str(char_index) + ");\n")
            back_part.append(tabs + "delete[] cha" + str(char_index) + ";\n")
            back_part.append(tabs + "\n")
            char_index = char_index + 1
            front = front + front_part
            back = back + back_part
        new_body = new_body[:2] + front + new_body[2:-1] + back + new_body[-1:]
        addings.append((do_data, new_body))
    return files, addings


def edit_PreTranslateMessage(cursor, files, main_name):
    trans_msg = cpp_parser.get_function_definition_by_name_in_mainFile(cursor, "PreTranslateMessage")
    if trans_msg is not None:
        # delete Parameter, not needed anymore, no need of method delete_param, due to overwriting params with add
        trans_msg.parameters = []
        # add new header
        trans_msg_header = files[main_name][trans_msg.start - 1]
        trans_msg_header = trans_msg_header.split("(", 1)[0] + "(std::string message, std::string wParam)" + trans_msg_header.rsplit(")", 1)[1]
        files[main_name] = files[main_name][:trans_msg.start - 1] + [trans_msg_header] + files[main_name][trans_msg.start:]
        trans_msg.add_parameter("message", "std::string", trans_msg.location)
        trans_msg.add_parameter("wParam", "std::string", trans_msg.location)
        # add new parameter to declaration in class
        files = add_params_to_class_declaration(trans_msg, trans_msg.node.semantic_parent, files)
        for idx, line in enumerate(files[main_name]):
            files[main_name][idx] = re.sub(r"pMsg->", "", line)
    return files


def edit_onOK(cursor, files_list, nms, new_functions):
    onOK = cpp_parser.get_function_definition_by_name_in_mainFile(cursor, "OnOK")
    onCancel = cpp_parser.get_function_definition_by_name_in_mainFile(cursor, "OnCancel")
    if onOK is not None:
        # get spacing of header or rather the line beyond with '{'
        match = re.match(r"^(\s)+\S", files_list[onOK.start])
        tabs = ""
        if match is not None:
            tabs = match.group(0)[:-1]
        tabs = tabs + "\t"
        # add Functions needed in OnOK
        files_list.insert(onOK.start + 1, tabs + '::SendStatusToServer("OK", this);\n')
        files_list.insert(onOK.start + 1, tabs + "DoDataExchange(true);\n")
    elif onCancel is not None:
        fhead = "void "
        if nms is not None:
            fhead = fhead + nms + "::"
        start, end = cpp_parser.get_function_start_end_in_main_file(cursor, "DoDataExchange")
        idx = 0
        if end != -1:
            idx = end
        files_list[idx:idx] = ["\n", fhead + "OnOK()\n", "{\n", '\t::SendStatusToServer("OK", this);\n', "\tDoDataExchange(true);\n", "}\n", "\n"]
        if nms not in new_functions:
            new_functions[nms] = []
        new_functions[nms].append("\tvoid OnOK();\n")
    return files_list, new_functions


def edit_onCancel(cursor, files_list, nms, new_functions):
    onCancel = cpp_parser.get_function_definition_by_name_in_mainFile(cursor, "OnCancel")
    onOK = cpp_parser.get_function_definition_by_name_in_mainFile(cursor, "OnOK")
    if onCancel is not None:
        # get spacing of header or rather the line beyond with '{'
        match = re.match(r"^(\s)+\S", files_list[onCancel.start])
        tabs = ""
        if match is not None:
            tabs = match.group(0)[:-1]
        tabs = tabs + "\t"
        # add Functions needed in OnCancel
        files_list.insert(onCancel.start + 1, tabs + '::SendStatusToServer("Cancel", this);\n')
    elif onOK is not None:
        fhead = "void "
        if nms is not None:
            fhead = fhead + nms + "::"
        start, end = cpp_parser.get_function_start_end_in_main_file(cursor, "DoDataExchange")
        idx = 0
        if end != -1:
            idx = end
        files_list[idx:idx] = ["\n", fhead + "OnCancel()\n", "{\n", '\t::SendStatusToServer("Cancel", this);\n', "}\n", "\n"]
        if nms not in new_functions:
            new_functions[nms] = []
        new_functions[nms].append("\tvoid OnCancel();\n")
    return files_list, new_functions


def add_JSON_methods(cursor, files_dict):
    classes = cpp_parser.get_class_definitions(cursor)
    classes.sort(key=lambda cl: cl.start, reverse=True)
    for cl in classes:
        if not cl.variables:
            continue
        get_head = "void to_json(json& j, const " + cl.name + "& obj)\n"
         # j = json{{"name", p.name}, {"address", p.address}, {"age", p.age}};
        get_func = [get_head, "{\n"]
        get_func.append('\tj["name"] = "' + cl.name + '";\n')
        set_head = "void from_json(const json& j, " + cl.name + "& obj)\n"
        set_func = [set_head, "{\n"]
        for vari in cl.variables:
            if vari.type == "int":
                match = re.search(r"(std::vector<.*>)(\s?)"+ re.escape(vari.name), files_dict[vari.location][vari.line-1])
                if match:
                    vari.type = match.group(1)
            if "*" in vari.type:
                base_type = ''.join(e for e in vari.type if e != "*" and e != " ")
                if "vector" in vari.type:
                    get_func.append("\t" + base_type + " tmp_" + vari.name + ";\n")
                    get_func.append("\tfor (unsigned i = 0; i < obj." + vari.name + ".size(); ++i) {\n")
                    get_func.append("\t\ttmp_" + vari.name + ".push_back(*obj." + vari.name + "[i]);\n")
                    get_func.append("\t}\n")
                    get_func.append('\tj["variables"]["' + vari.name + '"] = json(tmp_' + vari.name + ');\n')
                    set_func.append('\t' + base_type + ' tmp_' + vari.name + ' = j.at("variables").at("' + vari.name + '").get<' +
                                    base_type + '>();\n')
                    set_func.append("\t" + vari.type + " tmp_" + vari.name + "2;\n")
                    set_func.append("\tfor (unsigned i = 0; i < tmp_" + vari.name + ".size(); ++i) {\n")
                    set_func.append("\t\ttmp_" + vari.name + "2.push_back(&tmp_" + vari.name + "[i]);\n")
                    set_func.append("\t}\n")
                    set_func.append('\tobj.' + vari.name + ' = tmp_' + vari.name + '2;\n')
                else:
                    get_func.append("")
                    get_func.append('\tj["variables"]["' + vari.name + '"] = json(*obj.' + vari.name + ');\n')
                    set_func.append('\t' + base_type + ' tmp = j.at("variables").at("' + vari.name + '").get<' +
                                    base_type + '>();\n')
                    set_func.append('\tobj.' + vari.name + ' = &tmp;\n')
            else:
                get_func.append('\tj["variables"]["' + vari.name + '"] = json(obj.' + vari.name + ');\n')
                set_func.append('\tobj.' + vari.name + ' = j.at("variables").at("' + vari.name + '").get<' + vari.type + '>();\n')
        get_func.append("}\n")
        set_func.append("}\n")
        files_dict[cl.location][cl.end:cl.end] = ["\n", "\n"] + get_func + ["\n", "\n"] + set_func + ["\n"]
        # if cl.name not in new_functions:
        #     new_functions[cl.name] = []
        # new_functions[cl.name].append(get_head[:-1].replace(cl.name + "::", "") + ";\n")
        # new_functions[cl.name].append(set_head[:-1].replace(cl.name + "::", "") + ";\n")
    return files_dict


def build_expert_functions(cursor, files_dict, bodies):
    expert_functions = cpp_parser.get_function_definitions_at_file(cursor, cfg.EXPERT_FILE)
    classes = cpp_parser.get_class_definitions(cursor)
    for func in expert_functions:
        if "//RWMPFLAG:real_func" in files_dict[func.location][func.start+1]:
            continue
        match = re.match(r"^(\s)+\S", files_dict[func.location][func.start])
        tabs = ""
        if match is not None:
            tabs = match.group(0)[:-1]
        tabs = tabs + "\t"
        new_body = [tabs + "json j;\n", tabs + 'j["name"] = "' + func.name + '";\n']
        for para in func.parameters:
            if para.type == "int":
                match = re.search(r"(std::vector<.*>)(\s?)"+ re.escape(para.name), files_dict[para.location][para.line-1])
                if match:
                    para.type = match.group(1)
            if "*" in para.type:
                if "vector" in para.type:
                    new_body.append(tabs + "\n")
                    new_body.append(tabs + ''.join(e for e in para.type if e != "*" and e != " ") + " tmp_" + para.name + ";\n")
                    new_body.append(tabs + "for (unsigned i = 0; i < " + para.name + ".size(); ++i) {\n")
                    new_body.append(tabs + "\ttmp_" + para.name + ".push_back(*" + para.name + "[i]);\n")
                    new_body.append(tabs + "}\n")
                    new_body.append(tabs + 'j["arguments"]["' + para.name + '"] = json(tmp_' + para.name + ');\n')
                else:
                    new_body.append(tabs + 'j["arguments"]["' + para.name + '"] = json(*' + para.name + ');\n')
            else:
                new_body.append(tabs + 'j["arguments"]["' + para.name + '"] = json(' + para.name + ');\n')
        new_body.append(tabs + 'std::string s = j.dump(4);\n')
        new_body.append(tabs + "char* cha = new char[s.size() + 1];\n")
        new_body.append(tabs + "strcpy(cha, s.c_str());\n")
        if func.type == "void":
            new_body.append(tabs + "EM_ASM_({\n")
        else:
            new_body.append(tabs + "std::string rs = (char*)EM_ASM_INT({\n")
        new_body.append(tabs + "\tvar data_string = UTF8ToString($0);\n")
        new_body.append(tabs + '\tvar return_string = send_server_function(data_string);\n')
        if func.type != "void":
            new_body.append(tabs + '\tvar lengthBytes = lengthBytesUTF8(return_string)+1;\n')
            new_body.append(tabs + '\tvar stringOnWasmHeap = _malloc(lengthBytes);\n')
            new_body.append(tabs + '\tstringToUTF8(return_string, stringOnWasmHeap, lengthBytes+1);\n')
            new_body.append(tabs + '\treturn stringOnWasmHeap;\n')
        new_body.append(tabs + "},cha);\n")
        new_body.append(tabs + "delete[] cha;\n")
        if func.type != "void":
            new_body.append(tabs + 'j = json::parse(rs);\n')
            if func.type == "int":
                match = re.search(r"(std::vector<.*>)(\s?)"+ re.escape(func.name), files_dict[func.location][func.start-1])
                if match:
                    func.type = match.group(1)
            if "*" in func.type:
                base_type = ''.join(e for e in func.type if e != "*" and e != " ")
                if "vector" in func.type:
                    new_body.append(tabs + base_type + ' tmp_' + func.name + ' = j.at("returns").at("return").get<' + base_type + '>();\n')
                    new_body.append(tabs + func.type + " tmp_" + func.name + "2;\n")
                    new_body.append(tabs + "for (unsigned i = 0; i < tmp_" + func.name + ".size(); ++i) {\n")
                    new_body.append(tabs + "\ttmp_" + func.name + "2.push_back(&tmp_" + func.name + "[i]);\n")
                    new_body.append(tabs + "}\n")
                    new_body.append(tabs + 'return tmp_' + func.name + '2;\n')
                else:
                    new_body.append(tabs + base_type + ' r_base = j.at("returns").at("return").get<' + base_type + '>();\n')
                    new_body.append(tabs + "return &r_base;\n")
            else:
                new_body.append(tabs + 'return j.at("returns").at("return").get<' + func.type + '>();\n')
        bodies.append((func, new_body))
    return bodies


def write_new_bodies(cursor, files_dict, bodies):
    del_bodies = []
    for idx, tu in enumerate(bodies):
        tmp_func = tu[0]
        if "vector" in tmp_func.type:
            tmp_func.type = "int"
        func = cpp_parser.get_function_definition_by_obj(cursor, tmp_func)
        if func is not None:
            bodies[idx] = (func.location, func.start, func.end, tu[1])
        else:
            del_bodies.append(idx)
    del_bodies.sort(reverse=True)
    for d in del_bodies:
        del bodies[d]
    bodies.sort(key=lambda t: t[1], reverse=True)
    for tu in bodies:
        files_dict[tu[0]] = files_dict[tu[0]][:tu[1] + 1] + tu[3] + files_dict[tu[0]][tu[2] - 1:]
    return files_dict


def add_SendStatusToServer(file_list, mainspace):
    send_status = []
    index = -1
    for idx, line in enumerate(file_list):
        if ("OnOK(" in line or "OnCancel(" in line or "OnInitDialog(" in line) and "void" in line:
            index = idx
            break
    send_status.append("\n")
    send_status.append("void SendStatusToServer(std::string flag, " + mainspace + "* self)\n")
    send_status.append("{\n")
    send_status.append('\tjson j;\n')
    send_status.append('\tj["name"] = "SendStatusToServer";\n')
    send_status.append('\tj["arguments"]["flag"] = flag;\n')
    send_status.append('\tj["arguments"]["status"] = json(*self);\n')
    send_status.append('\tstd::string s = j.dump(4);\n')
    send_status.append("\tchar* cha = new char[s.size() + 1];\n")
    send_status.append("\tstrcpy(cha, s.c_str());\n")
    send_status.append("\tEM_ASM_({\n")
    send_status.append("\t\tvar data_string = UTF8ToString($0);\n")
    send_status.append('\t\tsend_server_function(data_string);\n')
    send_status.append("\t}, cha);\n")
    send_status.append("\tdelete[] cha;\n")
    send_status.append("}\n")
    send_status.append("\n")
    send_status.append("\n")
    file_list[index:index] = send_status
    return file_list


def collect_embinds(cursor, main_file, mainspace, message_maps, new_functions, files):
    embindings = {"mainspace": mainspace}
    functions = cpp_parser.get_function_definitions_at_file(cursor, main_file)
    embindings["constructors"] = [funci for funci in functions if funci.is_constructor()]
    embindings["functions"] = []
    for func in functions:
        if func.name == mainspace + "::OnOK" or func.name == mainspace + "::OnCancel" \
                or func.name == mainspace + "::OnInitDialog" or func.name == mainspace + "::PreTranslateMessage" \
                or any(func.spelling in line and not func.spelling + "::" in line for line in message_maps[mainspace]):
            embindings["functions"].append(func)
            cl = cpp_parser.Class(func.node.semantic_parent)
            if cl.name not in new_functions:
                new_functions[cl.name] = []
            delete_lines = []
            for idx, line in enumerate(files[cl.location]):
                if func.spelling in line and line.endswith(";\n"):
                    delete_lines.append((idx + 1, idx + 1, line))
            for tup in delete_lines:
                files[cl.location] = delete_lines_in_file(files[cl.location], tup[0], tup[1])
                new_functions[cl.name].append(tup[2])
    return embindings, new_functions


def add_embinds(embindings, file_list):
    file_list.append("\n")
    file_list.append("\n")
    file_list.append("//Binding code\n")
    file_list.append("EMSCRIPTEN_BINDINGS(rwmp_binds){\n")
    file_list.append("\tusing namespace emscripten;\n")
    file_list.append('\tclass_<' + embindings["mainspace"] + '>("' + embindings["mainspace"] + '")\n')
    if not embindings["constructors"]:
        file_list.append("\t\t.constructor()\n")
    else:
        for c in embindings["constructors"]:
            if not c.parameters:
                file_list.append("\t\t.constructor()\n")
            else:
                types = ""
                for p in c.parameters:
                    types = types + p.type + ", "
                types = types[:-2]
                file_list.append("\t\t.constructor<" + types + ">()\n")
    for f in embindings["functions"]:
        file_list.append('\t\t.function("' + f.spelling + '", &' + f.name + ')\n')
    file_list.append("\t\t;\n")
    file_list.append("}\n")
    file_list.append("\n")
    file_list.append("\n")
    return file_list


def copy_template_files(dest):
    template_path = os.getcwd() + "\\templates"
    template_files = os.listdir(template_path)
    for file_name in template_files:
        full_file_name = os.path.join(template_path, file_name)
        if (os.path.isfile(full_file_name)):
            shutil.copy(full_file_name, dest)


def copy_pretemplate_files(dest):
    template_path = os.getcwd() + "\\templates\\pre"
    template_files = os.listdir(template_path)
    for file_name in template_files:
        full_file_name = os.path.join(template_path, file_name)
        if (os.path.isfile(full_file_name)):
            shutil.copy(full_file_name, dest)


def has_constructor_without_parameters(constructors):
    if not constructors:
        return True
    else:
        for constructor in constructors:
            if not constructor.parameters:
                return True
    return False


def add_rwmp_init_to_js(embinds, message_maps, path):
    mainspace = embinds["mainspace"]
    # open rwmp.js
    file = io.open(path + "rwmp.js", mode="r", encoding="iso-8859-15")
    lines = file.readlines()
    file.close()
    #write rwmp_init()
    lines.append("\n")
    lines.append("\n")
    lines.append("function rwmp_init() {\n")
    lines.append("\ttry {\n")
    if has_constructor_without_parameters(embinds["constructors"]):
        lines.append("\t\trwmp =  new Module." + mainspace + "();\n")
    if any("OnInitDialog" in func.name for func in embinds["functions"]):
        lines.append("\t\trwmp.OnInitDialog();\n")
    if any("OnOK" in func.name for func in embinds["functions"]):
        lines.append('\t\tdocument.getElementById("IDOK").addEventListener("click", function(){\n')
        lines.append('\t\t\trwmp.OnOK();\n')
        lines.append("\t\t});\n")
    if any("OnCancel" in func.name for func in embinds["functions"]):
        lines.append('\t\tdocument.getElementById("IDCANCEL").addEventListener("click", function(){\n')
        lines.append('\t\t\trwmp.OnCancel();\n')
        lines.append("\t\t});\n")
    if mainspace in message_maps:
        for line in message_maps[mainspace]:
            if line.startswith("ON_BN_CLICKED"):
                button_id = line[line.index("(") + 1:line.index(",")]
                func = line[line.index(",") + 1:line.index(")")].strip()
                lines.append('\t\tdocument.getElementById("' + button_id + '").addEventListener("click", function(){\n')
                lines.append('\t\t\trwmp.' + func + '();\n')
                lines.append("\t\t});\n")
    lines.append('\t\talert("ReWaMP load succeeded.")')
    lines.append("\t}\n")
    lines.append("\tcatch (e) {\n")
    lines.append("\t\tsetTimeout(rwmp_init(), 5000);\n")
    lines.append("\t}\n")
    lines.append("}\n")
    lines.append("\n")
    lines.append("\n")
    #save rwmp.js
    file = io.open(path + "rwmp.js", mode="w+", encoding="iso-8859-15")
    file.writelines(lines)
    file.close()


def process_flags(files_dict):
    for file_name, file_content in files_dict.items():
        for idx, line in enumerate(file_content):
            # change strcpy to str.copy
            if "//RWMPFLAG:strcpy2string" in line:
                match = re.match(r"^(\s)+\S", line)
                tabs = ""
                if match is not None:
                    tabs = match.group(0)[:-1]
                dest, src = line.split("(", 1)[1].rsplit(")", 1)[0].split(",")
                tmp_line = tabs + dest.strip() + "=" + src.strip() + ";\n"
                file_content[idx] = tmp_line
                continue
        files_dict[file_name] = file_content
    return files_dict


def add_new_functions(new_functions, cursor, files):
    classes = []
    for class_name in new_functions:
        classes.append(cpp_parser.get_class_definition_by_name(cursor, class_name))
    classes.sort(key=lambda c: c.start, reverse=True)
    for cl in classes:
        files[cl.location][cl.end - 1:cl.end - 1] = ["\n", "public:\n"] + new_functions[cl.name]
    return files

def check_libclang_architecture():
    libclang_path = os.path.join(cfg.LLVM_PATH, "libclang.dll")
    libclang_path_86 = os.path.join(cfg.LLVM_PATH, "libclang_x86.dll")
    libclang_path_64 = os.path.join(cfg.LLVM_PATH, "libclang_x64.dll")
    if sys.maxsize > 2 ** 32 and os.path.isfile(libclang_path_64):
        if os.path.isfile(libclang_path):
            os.rename(libclang_path, libclang_path_86)
        os.rename(libclang_path_64, libclang_path)
    elif not sys.maxsize > 2 ** 32 and os.path.isfile(libclang_path_86):
        if os.path.isfile(libclang_path):
            os.rename(libclang_path, libclang_path_64)
        os.rename(libclang_path_86, libclang_path)

if __name__ == '__main__':
    # using the correct libclang
    check_libclang_architecture()
    # time measurement variables
    start_time = time.time()
    input_time = 0.0
    time_prints = []
    # main algorithm start
    files = init_file_dict()
    for main_file_name in cfg.MAIN_FILES:
        time_loop_start = time.time()
        main_cursor = cpp_parser.get_main_cursor(cfg.FILES_PATH + "\\" + main_file_name, True)
        related_path = cfg.FILES_PATH + "\\" + os.path.splitext(main_file_name)[0] + "\\"
        # related files
        other_names = [name for name in cfg.MAIN_FILES if not name == main_file_name]
        related_files_set, related_files_list = cpp_parser.get_related_files_in_dir(main_cursor, other_names)
        additional_files = []
        for key in files:
            if any(name.rsplit(".",1)[0] in key and key != name for name in related_files_list) and key not in related_files_set:
                additional_files.append(key)
        for name in additional_files:
            related_files_list.append(name)
            related_files_set.update([name])
        related_files = {k: files[k] for k in related_files_list}
        related_files[main_file_name] = related_files[main_file_name] + ["\n", "\n"]
        # includings
        includings = cpp_parser.get_include_directives(main_cursor, related_files_set)
        includings.sort(key=lambda i: i.line, reverse=True)
        related_files = delete_unknown_includings(includings, related_files, main_file_name)
        related_files[main_file_name] = add_includes_to_file(related_files[main_file_name], includings, cfg.HEADERS)
        # minor changes
        related_files, message_maps = minor_changes(related_files)
        del main_cursor
        save_file_dict(related_files, related_path)
        main_cursor2 = cpp_parser.get_main_cursor(related_path + main_file_name, False)
        new_bodies = []
        # deletion of unused params
        related_files = delete_unused_params(main_cursor2, related_files)
        # functions = cpp_parser.get_function_definitions(main_cursor2) # for testing
        # translate PreTranslateMessage from MFC to WASM compatibility
        related_files = edit_PreTranslateMessage(main_cursor2, related_files, main_file_name)
        # doDataExchange edit
        related_files, new_bodies = edit_DoDataExchange(main_cursor2, related_files, new_bodies, main_file_name)
        # getting main_namespace
        classes = cpp_parser.get_class_definitions(main_cursor2)
        main_namespace = None
        for c in classes:
            if main_file_name.split(".", 1)[0] in c.name:
                main_namespace = c.name
        new_funcs = {}
        # translate OnOK from MFC to WASM compatibility
        related_files[main_file_name], new_funcs = edit_onOK(main_cursor2, related_files[main_file_name],
                                                             main_namespace, new_funcs)
        del main_cursor2
        save_file_dict(related_files, related_path)
        cancel_cursor = cpp_parser.get_main_cursor(related_path + main_file_name, False)
        # translate OnCancel from MFC to WASM compatibility
        related_files[main_file_name], new_funcs = edit_onCancel(cancel_cursor, related_files[main_file_name],
                                                                 main_namespace, new_funcs)
        del cancel_cursor
        save_file_dict(related_files, related_path)
        input_cursor = cpp_parser.get_main_cursor(related_path + main_file_name, False)
        # get false declarations for guessing where expert should take a look
        decls = cpp_parser.get_variables_and_fields(input_cursor)
        false_decls = []
        for decl in decls:
            line = related_files[decl.location][decl.line-1]
            if not re.search(r"([^0-9a-zA-Z])" + re.escape(decl.type.split(" ", 1)[0]) + r".*" + re.escape(decl.name), line):
                decl.real_type = line.split(decl.name, 1)[0].strip().replace("\t", " ")
                if (decl.real_type[-1] == "*" or decl.real_type[-1] == "&") and decl.real_type[-2] != " ":
                    decl.real_type = decl.real_type[:-1] + " " + decl.real_type[-1]
                false_decls.append(decl)
        # TODO show unknown function calls
        del input_cursor
        input_start = time.time()
        time_prints.append("time elapsed after action 3: {:.2f}s".format(input_start - time_loop_start))
        print("")
        print("Please add expert knowledge. Therefore read the code and copy the expert file with the name '"
              + cfg.EXPERT_FILE + "' to the working dir at '" + related_path + "'.")
        print("Following could help:")
        for dec in false_decls:
            print("<Unknown Declaration, File: " + dec.location + ", Line: " + str(dec.line) + ">")
        input("Press Enter to continue...")
        input_stop = time.time()
        copy_pretemplate_files(related_path)
        related_files = load_intermediate_file_dict(related_path)
        expert_cursor = cpp_parser.get_main_cursor(related_path + main_file_name, False)
        new_bodies = build_expert_functions(expert_cursor, related_files, new_bodies)
        # adding JSON stringify Methods where needed
        related_files = add_JSON_methods(expert_cursor, related_files)
        # process all known flags in comments
        related_files = process_flags(related_files)
        del expert_cursor
        save_file_dict(related_files, related_path)
        collect_cursor = cpp_parser.get_main_cursor(related_path + main_file_name, False)
        # collect embinds
        embinds, new_funcs = collect_embinds(collect_cursor, main_file_name, main_namespace, message_maps, new_funcs,
                                             related_files)
        del collect_cursor
        save_file_dict(related_files, related_path)
        func_cursor = cpp_parser.get_main_cursor(related_path + main_file_name, False)
        related_files = add_new_functions(new_funcs, func_cursor, related_files)
        del func_cursor
        save_file_dict(related_files, related_path)
        body_cursor = cpp_parser.get_main_cursor(related_path + main_file_name, False)
        # write new bodies to functions, clang parser won't parse all files correct after this
        related_files = write_new_bodies(body_cursor, related_files, new_bodies)
        # write SendStatusToServer Function to rwmpExpert
        related_files[main_file_name] = add_SendStatusToServer(related_files[main_file_name], main_namespace)
        # add embinds
        related_files[main_file_name] = add_embinds(embinds, related_files[main_file_name])
        del body_cursor
        save_file_dict(related_files, related_path)
        # copy template Files to main_file_dir
        copy_template_files(related_path)
        # add rwmp_init method to rwmp.js
        add_rwmp_init_to_js(embinds, message_maps, related_path)
        time_6 = time.time()
        time_prints.append("time elapsed after action 6: {:.2f}s".format(time_6 - input_stop))
        '''
        execute EMSCRIPTEN
        '''
        done = False
        while not done:
            output_name = main_file_name[:main_file_name.rfind('.')]
            try:
                subprocess.check_call("""emsdk_env --build=Release & emcc --bind """ + related_path + main_file_name
                                      + """ -o """ + related_path + output_name + """.js -s WASM=1 -std=c++11 -O2 -s ALLOW_MEMORY_GROWTH=1""",
                                  cwd=cfg.EMSCRIPTEN_PATH, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                done = True
            except subprocess.CalledProcessError as err:
                print("Error at compiling " + main_file_name + " to WASM:\n{0}".format(err))
                input("Press Enter to continue...")
                time_6 = time.time()
        time_prints.append("time elapsed after action 7: {:.2f}s".format(time.time() - time_6))
        input_time = input_time + (input_stop - input_start)
    for t in time_prints:
        print(t)
    print("time elapsed overall: {:.2f}s".format((time.time() - start_time) - input_time))

