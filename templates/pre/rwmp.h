#include <time.h>
#include <map>
#include <iterator>

void MessageBox(char *text, char *head)
{
	
}

void NextDlgCtrl()
{
    
}

void PrevDlgCtrl()
{
    
}

void SetFocus(std::string elem)
{
    
}

std::string GetFocus()
{
    
}

bool CheckFocus(std::string elem)
{
    
}

int GetDay_RWMP()
{
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);
    int day = aTime->tm_mday;
    return day;
}

int GetMonth_RWMP()
{
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);
    int month = aTime->tm_mon + 1; // Month is 0 - 11, add 1 to get a jan-dec 1-12 concept
    return month;
}

int GetYear_RWMP()
{
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);
    int year = aTime->tm_year + 1900; // Year is # years since 1900
    return year;
}

std::string getInnerText(std::string id)
{
    
}

void setInnerText(std::string id, std::string text)
{
    
}

void showElement(std::string id, bool show)
{
    
}

class RWMPComboBox
{
public:
    std::map<std::string, int> dataMap;
    std::string id;
    std::string curSel;

    RWMPComboBox(std::string ido)
    {
        id = ido;
        curSel = std::string("");
    }

    RWMPComboBox()
    {
        id = std::string("");
        curSel = std::string("");
    }

    std::string AddString(std::string key)
    {
        
    }

    void SetItemData(std::string key, int data)
    {
        std::map<std::string, int>::iterator it = dataMap.find(key);
        if (it != dataMap.end()) {
            it->second = data;
        }
    }

    std::string GetCurSel()
    {
        
    }

    int GetItemData(std::string key)
    {
       std::map<std::string, int>::iterator it = dataMap.find(key);
        if (it != dataMap.end()) {
            return it->second;
        }
        return -1;
    }
};

void to_json(json& j, const RWMPComboBox& p) {
    j["name"] = "RWMPComboBox";
	j["variables"]["id"] = json(p.id);
	j["variables"]["curSel"] = json(p.curSel);
	j["variables"]["dataMap"] = json(p.dataMap);
}

void from_json(const json& j, RWMPComboBox& p) {
	p.id = j.at("variables").at("id").get<std::string>();
	p.curSel = j.at("variables").at("curSel").get<std::string>();
	p.dataMap = j.at("variables").at("dataMap").get<std::map<std::string, int>>();
}

class RWMPCheckBox
{
public:
    std::string id;
    int checked;

    RWMPCheckBox(std::string ido)
    {
        id = ido;
        checked = 0;
    }

    RWMPCheckBox()
    {
        id = std::string("");
        checked = 0;
    }

    int GetCheck()
    {
        
    }
};

void to_json(json& j, const RWMPCheckBox& p) {
    j["name"] = "RWMPCheckBox";
	j["variables"]["id"] = json(p.id);
	j["variables"]["checked"] = json(p.checked);
}

void from_json(const json& j, RWMPCheckBox& p) {
	p.id = j.at("variables").at("id").get<std::string>();
	p.checked = j.at("variables").at("checked").get<int>();
}



