#include <time.h>
#include <map>
#include <iterator>

void MessageBox(char *text, char *head)
{
	EM_ASM_({
		var text = UTF8ToString($0);
		var head = UTF8ToString($1);
		alert(text);
	}, text, head);
}

void NextDlgCtrl()
{
    EM_ASM(
        alert('NextDlgCtrl is called, yet not implemented.');
    );
}

void PrevDlgCtrl()
{
    EM_ASM(
        alert('PrevDlgCtrl is called, yet not implemented.');
    );
}

void SetFocus(std::string elem)
{
    char* cha = new char[elem.size() + 1];
	strcpy(cha, elem.c_str());
	EM_ASM_({
		var data_string = UTF8ToString($0);
		alert('SetFocus is called, yet not implemented.');
	}, cha);
	delete[] cha;
}

std::string GetFocus()
{
    std::string rs = (char*)EM_ASM_INT({
		var return_string = get_focus();
		var lengthBytes = lengthBytesUTF8(return_string)+1;
		var stringOnWasmHeap = _malloc(lengthBytes);
		stringToUTF8(return_string, stringOnWasmHeap, lengthBytes+1);
		return stringOnWasmHeap;
	}, 0);
	return rs;
}

bool CheckFocus(std::string elem)
{
    char* cha = new char[elem.size() + 1];
	strcpy(cha, elem.c_str());
	EM_ASM_({
		var data_string = UTF8ToString($0);
		alert('CheckFocus is called, yet not implemented.');
	}, cha);
	delete[] cha;
    return true;
}

int GetDay_RWMP()
{
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);
    int day = aTime->tm_mday;
    return day;
}

int GetMonth_RWMP()
{
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);
    int month = aTime->tm_mon + 1; // Month is 0 - 11, add 1 to get a jan-dec 1-12 concept
    return month;
}

int GetYear_RWMP()
{
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);
    int year = aTime->tm_year + 1900; // Year is # years since 1900
    return year;
}

std::string getInnerText(std::string id)
{
    char* cha = new char[id.size() + 1];
	strcpy(cha, id.c_str());
    std::string rs = (char*)EM_ASM_INT({
        var data_string = UTF8ToString($0);
		var return_string = get_inner_text(data_string);
		var lengthBytes = lengthBytesUTF8(return_string)+1;
		var stringOnWasmHeap = _malloc(lengthBytes);
		stringToUTF8(return_string, stringOnWasmHeap, lengthBytes+1);
		return stringOnWasmHeap;
	}, cha);
	delete[] cha;
	return rs;
}

void setInnerText(std::string id, std::string text)
{
    char* cha = new char[id.size() + 1];
	strcpy(cha, id.c_str());
	char* cha2 = new char[text.size() + 1];
	strcpy(cha2, text.c_str());
    EM_ASM_({
        var id = UTF8ToString($0);
        var text = UTF8ToString($1);
		set_inner_text(id, text);
	}, cha, cha2);
	delete[] cha;
	delete[] cha2;
}

void showElement(std::string id, bool show)
{
    int i = 0;
    if(show == true){
        i = 1;
    }
    char* cha = new char[id.size() + 1];
	strcpy(cha, id.c_str());
    EM_ASM_({
        var id = UTF8ToString($0);
        var show = true;
        if ($1 === 0){
            show = false;
        }
		show_element(id, show);
	}, cha, i);
	delete[] cha;
}

class RWMPComboBox
{
public:
    std::map<std::string, int> dataMap;
    std::string id;
    std::string curSel;

    RWMPComboBox(std::string ido)
    {
        id = ido;
        curSel = std::string("");
    }

    RWMPComboBox()
    {
        id = std::string("");
        curSel = std::string("");
    }

    std::string AddString(std::string key)
    {
        dataMap.insert(std::make_pair(key, 0));
        char* cha = new char[key.size() + 1];
        strcpy(cha, key.c_str());
        char* cha2 = new char[id.size() + 1];
        strcpy(cha2, id.c_str());
        EM_ASM_({
            var key = UTF8ToString($0);
            var id = UTF8ToString($1);
            add_option_to_select(id, key);
        }, cha, cha2);
        delete[] cha;
        delete[] cha2;
        return key;
    }

    void SetItemData(std::string key, int data)
    {
        std::map<std::string, int>::iterator it = dataMap.find(key);
        if (it != dataMap.end()) {
            it->second = data;
        }
    }

    std::string GetCurSel()
    {
        char* cha = new char[id.size() + 1];
        strcpy(cha, id.c_str());
        std::string rs = (char*)EM_ASM_INT({
            var id = UTF8ToString($0);
            var return_string = get_current_selection(id);
            var lengthBytes = lengthBytesUTF8(return_string)+1;
            var stringOnWasmHeap = _malloc(lengthBytes);
            stringToUTF8(return_string, stringOnWasmHeap, lengthBytes+1);
            return stringOnWasmHeap;
        }, cha);
        delete[] cha;
        curSel = rs;
        return rs;
    }

    int GetItemData(std::string key)
    {
       std::map<std::string, int>::iterator it = dataMap.find(key);
        if (it != dataMap.end()) {
            return it->second;
        }
        return -1;
    }
};

void to_json(json& j, const RWMPComboBox& p) {
    j["name"] = "RWMPComboBox";
	j["variables"]["id"] = json(p.id);
	j["variables"]["curSel"] = json(p.curSel);
	j["variables"]["dataMap"] = json(p.dataMap);
}

void from_json(const json& j, RWMPComboBox& p) {
	p.id = j.at("variables").at("id").get<std::string>();
	p.curSel = j.at("variables").at("curSel").get<std::string>();
	p.dataMap = j.at("variables").at("dataMap").get<std::map<std::string, int>>();
}

class RWMPCheckBox
{
public:
    std::string id;
    int checked;

    RWMPCheckBox(std::string ido)
    {
        id = ido;
        checked = 0;
    }

    RWMPCheckBox()
    {
        id = std::string("");
        checked = 0;
    }

    int GetCheck()
    {
        char* cha = new char[id.size() + 1];
        strcpy(cha, id.c_str());
        int rs = EM_ASM_INT({
            var id = UTF8ToString($0);
            return check_status(id);
        }, cha);
        checked = rs;
        return rs;
    }
};

void to_json(json& j, const RWMPCheckBox& p) {
    j["name"] = "RWMPCheckBox";
	j["variables"]["id"] = json(p.id);
	j["variables"]["checked"] = json(p.checked);
}

void from_json(const json& j, RWMPCheckBox& p) {
	p.id = j.at("variables").at("id").get<std::string>();
	p.checked = j.at("variables").at("checked").get<int>();
}



