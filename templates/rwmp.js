

function get_string_of_element(id) {
    if (!isNaN(id)){
        id = document.querySelector('[data-rwmpId="' + id + '"]').id
    }
    var elem = document.getElementById(id);
    if (elem instanceof HTMLInputElement && elem.type === "text"){
        return get_value_of_element(id);
    }
    else{
        return get_inner_text(id);
    }
}

function set_string_of_element(id, val) {
    if (!isNaN(id)){
        id = document.querySelector('[data-rwmpId="' + id + '"]').id
    }
    var elem = document.getElementById(id);
    if (elem instanceof HTMLInputElement && elem.type === "text"){
        set_value_of_element(id, val);
    }
    else{
        set_inner_text(id, val);
    }
}

function get_value_of_element(id) {
    if (!isNaN(id)){
        id = document.querySelector('[data-rwmpId="' + id + '"]').id
    }
    return document.getElementById(id).value;
}

function set_value_of_element(id, val) {
    if (!isNaN(id)){
        id = document.querySelector('[data-rwmpId="' + id + '"]').id
    }
    document.getElementById(id).value = val;
}

function send_server_function(data) {
    console.log(data);
    var obj = JSON.parse(data);
    if(obj.name === "ZMSGetNeuPatInPattern"){
    	return '{"returns": {"return": 42}}';
	}
    return '{"returns": {"return": "42"}}';
}

function get_focus() {
    return document.activeElement.id;
}

function get_inner_text(id) {
    if (!isNaN(id)){
        id = document.querySelector('[data-rwmpId="' + id + '"]').id
    }
    return document.getElementById(id).innerText;
}


function set_inner_text(id, text) {
    if (!isNaN(id)){
        id = document.querySelector('[data-rwmpId="' + id + '"]').id
    }
    document.getElementById(id).innerText = text;
}

function show_element(id, show) {
    if (!isNaN(id)){
        id = document.querySelector('[data-rwmpId="' + id + '"]').id
    }
    if (show === true){
        document.getElementById(id).style.visibility = "visible";
    }
    else {
        document.getElementById(id).style.visibility = "hidden";
    }
}

function add_option_to_select(id, key){
    var sel = document.getElementById(id);
    var opt = document.createElement("option");
    opt.text = key;
    sel.add(opt);
}

function get_current_selection(id) {
    if (!isNaN(id)){
        id = document.querySelector('[data-rwmpId="' + id + '"]').id
    }
    var sel = document.getElementById(id);
    if (sel.selectedIndex === -1) {
        return "";
    }
    return sel.options[sel.selectedIndex].text;
}

function check_status(id){
    if (!isNaN(id)){
        id = document.querySelector('[data-rwmpId="' + id + '"]').id
    }
    var checked = document.getElementById(id).checked;
    if (checked){
        return 1;
    }
    return 0;
}


