WASM-T
========

The **WASM-T**\ ransformation-Tool of the ReWaMP Process.


Installation
------------

1.  Clone Git Repository with git client or per terminal

2.  Install Python3, 32-bit is recommended. Should work with 64-bit, but libclang does not completely support 64-bit.

3.  Install emsdk portable version.

4.  Configure ``cfg.py`` with the correct pathes and file informations.

5.  Start it via console or within IDE and Use it!


Notes
-----

-   ``clang/`` is the directory of libclang's python bindings.

-   ``libclang.dll``, ``libclang_x64.dll`` and ``libclang_x86.dll`` are the possible names for the libclang DLL. Hereby ``libclang.dll`` is the last used DLL, ``libclang_x64.dll`` is the lastly unused 64-bit version and ``libclang_x86.dll`` is the lastly unused 32-bit version. Note, that WASM-T is renaming the DLLs accordingly to the used python architecture version.

-	``myCal/`` is a first example to migrate.

-	``intermediate/`` are the intermediate results of the migration engineer for each main file in ``myCal/``.
