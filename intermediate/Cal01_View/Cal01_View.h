//Thomas
#pragma once

#include "Calendar.cpp"

// Cal01_View-Dialogfeld
#define WM_MYMESSAGE	WM_APP+1 //defines command for messaging system
class Cal01_View 
{

private:
	virtual bool OnInitDialog();

public:
	Calendar *cal;
	std::string monthAndYear;
	std::string buttonDate;
	std::string outputLog;
	Cal01_View();   // Standardkonstruktor
	virtual ~Cal01_View();
	void updateText();
	void showCal();
	int getDayFromButton();
	void showDay(int, int, int);
	void OnBnClickedButtonPreviousmonth();
	void OnBnClickedButtonNextmonth();
	void OnDateClick();
	void OnBnClickedButtonAdddate();
	//#ifdef AFX_DESIGN_TIME
	//#endif

protected:
	virtual void DoDataExchange(bool direction);    // DDX/DDV-Unterst�tzung
};
