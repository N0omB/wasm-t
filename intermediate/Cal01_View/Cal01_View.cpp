//Thomas
// Cal01_View.cpp: Implementierungsdatei
#include <string>
#include <stdio.h>
#include <cstring>
#include <emscripten.h>
#include <emscripten/bind.h>
#include "json.hpp"

using json = nlohmann::json;

#include "rwmp.h"
#include "rwmpExpert.h"
//
//
#include "Cal01_View.h"

// Cal01_View-Dialogfeld


Cal01_View::Cal01_View()
{
	//text on dialog window
	monthAndYear = std::string("");
	buttonDate = std::string("");
	outputLog = std::string("");
	//creates new view to create new appointment

	//creates new calendar
	cal = new Calendar();
}

Cal01_View::~Cal01_View()
{
}

void Cal01_View::DoDataExchange(bool direction)
{
	//sends data to dialog
	DDX_Text(pDX, IDC_TEXT_MonthYear, monthAndYear);
	DDX_Text(pDX, IDC_BtoD, buttonDate);
	DDX_Text(pDX, IDC_STATIC_LOG, outputLog);
}



// Cal01_View-Meldungshandler

bool Cal01_View::OnInitDialog()
{
	//initialize dialog windows

	//create database

	//create view which holds appointments for a selected day

	//register view to model and set controller

	//set month and year on dialog window
	monthAndYear = cal->getMonthString(cal->getMonth()) + std::string(" ") + std::to_string(cal->getYear());
	DoDataExchange(false);

	//debug

	return true;
}



//get previous month
void Cal01_View::OnBnClickedButtonPreviousmonth()
{
	cal->previousMonth();
	showCal();
	updateText();	
}

//get next month
void Cal01_View::OnBnClickedButtonNextmonth()
{
	cal->nextMonth();
	showCal();
	updateText();
}

//updates text after switching month
void Cal01_View::updateText() {
	monthAndYear = cal->getMonthString(cal->getMonth()) + std::string(" ") + std::to_string(cal->getYear());
	DoDataExchange(false);
}

//initializes calendar
void Cal01_View::showCal() {
	int start = cal->getStartDay(cal->getMonth(), cal->getYear()); //which day is the first
	int days = cal->getDaysPerMonth(cal->getMonth());
	int counter = 1;
	//loop over buttons which represent a day
	for (int i = 1003; i < 1045; i++) {
		std::string id = std::to_string(i);
		if (start == 1 && counter <= days) {
			::showElement(id, true);
			::setInnerText(id, cal->getCalDaysAsString(counter));
			counter++;
		}
		else {
			start--;
			::showElement(id, false);
		}
	}
}

//click on a day
void Cal01_View::OnDateClick()
{

	int year = cal->getYear();
	int month = cal->getMonth();
	int day = getDayFromButton();

	showDay(day, month, year);

	showAppointmentsModal();
}

//get day from clicked button
int Cal01_View::getDayFromButton() {
	int buttonTextDay;

	std::string focus = ::GetFocus(); //which Button was pressed

	buttonTextDay = std::stoi(::getInnerText(focus)); //std::string to Int
	return buttonTextDay;
}

//show selected day on dialog
void Cal01_View::showDay(int d, int m, int y) {
	cal->setDay(d);
	cal->setMonth(m);
	cal->setYear(y);
	buttonDate = std::to_string(d) + std::string(".") + std::to_string(m) + std::string(".") + std::to_string(y);
	DoDataExchange(false);
}

//create new view to create new appointment
void Cal01_View::OnBnClickedButtonAdddate()
{
	int day = cal->getDay();
	int month = cal->getMonth();
	int year = cal->getYear();
	gotoDateView();
}

