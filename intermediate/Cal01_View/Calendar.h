//Thomas
#pragma once


class Calendar
{
public:
	int day;
	int year;
	int month;
	Calendar();
	~Calendar();
	//getter
	const int getDay();
	const int getMonth();
	const int getYear();
	//setter
	void setDay(int);
	void setMonth(int);
	void setYear(int);
	//methods
	void previousMonth();
	void nextMonth();
	std::string getMonthString(int);
	std::string getCalDaysAsString(int);
	int getMonthDigit(int);
	int getDaysPerMonth(int);
	bool isLeapYear(int);
	int getStartDay(int, int);
};