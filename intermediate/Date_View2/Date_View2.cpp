//Thomas
// Date_View2.cpp: Implementierungsdatei
//
#include <string>
#include <stdio.h>
#include <cstring>
#include <emscripten.h>
#include <emscripten/bind.h>
#include "json.hpp"

using json = nlohmann::json;

#include "rwmp.h"
#include "rwmpExpert.h"

#include "Date_View2.h"

// Date_View2-Dialogfeld


Date_View2::Date_View2()
{
	//initialze datastructures for database queries
	this->roomEntities = ::getRoomEntities();
	this->doctorEntities = ::getDoctorEntities();
	this->patientEntities = ::getPatientEntities();
}

void Date_View2::DoDataExchange(bool direction)
{
	//maps dialog elements to datastructures
	DDX_Control(pDX, IDC_COMBO1_DOCTOR, comboboxDoctor);
	DDX_Control(pDX, IDC_COMBO2_ROOM, comboboxRoom);
	DDX_Control(pDX, IDC_COMBO3_PATIENT, comboboxPatient);
	DDX_Control(pDX, IDC_COMBO4_START, comboboxStart);
	DDX_Control(pDX, IDC_COMBO5_DURATION, comboboxDuration);
	DDX_Control(pDX, IDC_CHECK1, checkMonday);
	DDX_Control(pDX, IDC_CHECK2, checkTuesday);
	DDX_Control(pDX, IDC_CHECK3, checkWednesday);
	DDX_Control(pDX, IDC_CHECK4, checkThursday);
	DDX_Control(pDX, IDC_CHECK5, checkFriday);
}


bool Date_View2::OnInitDialog()
{
	//initialize comboboxes with data from database
	std::string i = ""; //Position in jeweiliger Combobox
	for (std::vector<DoctorEntity>::iterator doctorIterator = this->doctorEntities.begin(); doctorIterator != this->doctorEntities.end(); ++doctorIterator) {
		std::string name = doctorIterator->getName();
		i = comboboxDoctor.AddString(name);
		comboboxDoctor.SetItemData(i, doctorIterator->getId());
	}
	for (std::vector<RoomEntity>::iterator roomIterator = this->roomEntities.begin(); roomIterator != this->roomEntities.end(); ++roomIterator) {
		std::string description = roomIterator->getDescription();
		i = comboboxRoom.AddString(description);
		comboboxRoom.SetItemData(i, roomIterator->getId());
	}
	for (std::vector<PatientEntity>::iterator patientIterator = this->patientEntities.begin(); patientIterator != this->patientEntities.end(); ++patientIterator) {
		std::string name = patientIterator->getName();
		i = comboboxPatient.AddString(name);
		comboboxPatient.SetItemData(i, patientIterator->getId());
	}

	//initialize combobox with durations
	i = comboboxDuration.AddString(std::string("5"));
	comboboxDuration.SetItemData(i, 5);
	i = comboboxDuration.AddString(std::string("10"));
	comboboxDuration.SetItemData(i, 10);
	i = comboboxDuration.AddString(std::string("15"));
	comboboxDuration.SetItemData(i, 15);
	i = comboboxDuration.AddString(std::string("30"));
	comboboxDuration.SetItemData(i, 30);

	//initialize combobox with starttimes
	i = comboboxStart.AddString(std::string("08:00"));
	comboboxStart.SetItemData(i, 800);
	i = comboboxStart.AddString(std::string("09:00"));
	comboboxStart.SetItemData(i, 900);
	i = comboboxStart.AddString(std::string("10:00"));
	comboboxStart.SetItemData(i, 1000);
	i = comboboxStart.AddString(std::string("11:00"));
	comboboxStart.SetItemData(i, 1100);
	i = comboboxStart.AddString(std::string("12:00"));
	comboboxStart.SetItemData(i, 1200);
	i = comboboxStart.AddString(std::string("13:00"));
	comboboxStart.SetItemData(i, 1300);
	i = comboboxStart.AddString(std::string("14:00"));
	comboboxStart.SetItemData(i, 1400);
	i = comboboxStart.AddString(std::string("15:00"));
	comboboxStart.SetItemData(i, 1500);
	i = comboboxStart.AddString(std::string("16:00"));
	comboboxStart.SetItemData(i, 1600);

	DoDataExchange(false);
	return true;
}

void Date_View2::OnOK()
{
	//Auswahl von Arzt, Patient, Doctor in Combobox
	std::string selectedDoctor, selectedPatient, selectedRoom;
	selectedDoctor = comboboxDoctor.GetCurSel();
	selectedDoctor = comboboxDoctor.GetItemData(selectedDoctor);

	selectedPatient = comboboxPatient.GetCurSel();
	selectedPatient = comboboxPatient.GetItemData(selectedPatient);

	selectedRoom = comboboxRoom.GetCurSel();
	selectedRoom = comboboxRoom.GetItemData(selectedRoom);

	//Auswahl der Tage
	bool days[7];
	days[5] = false; //Samstag
	days[6] = false; //Sonntag
	if (checkMonday.GetCheck() == 0) {
		days[0] = false;
	} else
	{
		days[0] = true;
	}
	if (checkTuesday.GetCheck() == 0) {
		days[1] = false;
	} else {
		days[1] = true;
	}
	if (checkWednesday.GetCheck() == 0) {
		days[2] = false;
	}
	else {
		days[2] = true;
	}
	if (checkThursday.GetCheck() == 0) {
		days[3] = false;
	}
	else {
		days[3] = true;
	}
	if (checkFriday.GetCheck() == 0) {
		days[4] = false;
	}
	else {
		days[4] = true;
	}

	//Auswahl von Zeitraum und Dauer
	std::string selectedDuration = comboboxDuration.GetCurSel();
	selectedDuration = comboboxDuration.GetItemData(selectedDuration);	

	std::string selectedStartTime = comboboxStart.GetCurSel();
	selectedStartTime = comboboxStart.GetItemData(selectedStartTime);

	DoDataExchange(true);
	::SendStatusToServer("OK", this);

	DoDataExchange(false);
}


void Date_View2::OnCancel()
{
	::SendStatusToServer("Cancel", this);
	//send message to main windows
}

