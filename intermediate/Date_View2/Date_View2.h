//Thomas
#pragma once

#include <vector>

// Date_View2-Dialogfeld
#define WM_MYMESSAGE	WM_APP+1 //defines command for messaging system
class Date_View2 
{

public:
	Date_View2();
	virtual ~Date_View2();
	RWMPComboBox comboboxStart;
	RWMPComboBox comboboxEnd;
	RWMPComboBox comboboxDuration;
	RWMPComboBox comboboxDoctor;
	RWMPComboBox comboboxRoom;
	RWMPComboBox comboboxPatient;
	RWMPCheckBox checkMonday;
	RWMPCheckBox checkTuesday;
	RWMPCheckBox checkWednesday;
	RWMPCheckBox checkThursday;
	RWMPCheckBox checkFriday;
	void OnOK();
	void OnCancel();
	std::vector<DoctorEntity> doctorEntities;
	std::vector<PatientEntity> patientEntities;
	std::vector<RoomEntity> roomEntities;

private:
	virtual bool OnInitDialog();

protected:
	virtual void DoDataExchange(bool direction);    // DDX/DDV-Unterst�tzung
};
