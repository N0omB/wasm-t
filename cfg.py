#!/usr/bin/env python

import os


'''
here changes are need often
'''
#FILES_PATH = r"Z:\wasmT\myCalMig"
FILES_PATH = r"Z:\Evaluation\Stefan"
MAIN_FILES = ["Cal01_View.cpp", "Date_View2.cpp"]

'''
here changes are need only sometimes
'''
EXPERT_FILE = "rwmpExpert.h"

HEADERS = [("string", "<", ">"), ("stdio.h", "<", ">"), ("cstring", "<", ">"), ("vector", "<", ">"), ("emscripten.h", '<', '>'),
           ("emscripten/bind.h", '<', '>'), ("json.hpp", '"', '"'), ("rwmp.h", '"', '"'), (EXPERT_FILE, '"', '"')]
		   
EMSCRIPTEN_PATH = r"W:\emsdk"

'''
here changes are needed neraby never
'''
LLVM_PATH=os.getcwd()

# cf. http://en.cppreference.com/w/cpp/header minus experimental libs and minus filesystem and anything below
STANDARD_LIBS = {"algorithm", "any", "array", "atomic", "bitset", "cassert", "cctype", "cerrno", "cfenv", "cfloat",
                 "charconv", "chrono", "cinttypes", "climit", "clocale", "cmath", "codecvt", "compare", "complex",
                 "condition_variable", "csetjump", "csignal", "cstdarg", "cstddef", "cstdint", "cstdio", "cstdlib",
                 "cstring", "ctime", "cuchar", "cwchar", "cwtype", "deque", "exception", "execution", "forward_list",
                 "fstream", "functional", "future", "iomanip", "ios", "iostream", "istream", "iterator", "limits",
                 "list", "locale", "map", "memory", "memory_resource", "mutex", "new", "numeric", "optional",
                 "ostream", "queue", "random", "ratio", "regex", "set", "scoped_allocator", "shared_mutex", "sstream",
                 "stack", "stdexcept", "stdio.h", "streambuf", "string", "string_view", "strstream", "syncstream",
                 "system_error", "thread", "time.h", "tuple", "typeindex", "typeinfo", "type_traits", "unordered_map",
                 "unordered_set", "utility", "valarray", "variant", "vector"}

