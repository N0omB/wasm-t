//Thomas
// Cal01_View.cpp: Implementierungsdatei
//
//
#include "stdafx.h"
#include "MyCal.h"
#include "Cal01_View.h"
#include "afxdialogex.h"

// Cal01_View-Dialogfeld

IMPLEMENT_DYNAMIC(Cal01_View, CDialog)

Cal01_View::Cal01_View(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_CAL01_DIALOG, pParent)
{
    //text on dialog window
	monthAndYear = _T("");
	buttonDate = _T("");
	outputLog = _T("");
	//creates new view to create new appointment
	date_View = new Date_View2();
	//creates new view that shows appointments for selected day
	appointments = new AppointmentsView();
	//creates new calendar
	cal = new Calendar();
}

Cal01_View::~Cal01_View()
{
}

void Cal01_View::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//sends data to dialog
	DDX_Text(pDX, IDC_TEXT_MonthYear, monthAndYear);
	DDX_Text(pDX, IDC_BtoD, buttonDate);
	DDX_Text(pDX, IDC_STATIC_LOG, outputLog);
}


BEGIN_MESSAGE_MAP(Cal01_View, CDialog)
	//maps buttons on dialog to functions
	ON_BN_CLICKED(IDC_BUTTON_PREVIOUSMONTH, &Cal01_View::OnBnClickedButtonPreviousmonth)
	ON_BN_CLICKED(IDC_BUTTON_NEXTMONTH, &Cal01_View::OnBnClickedButtonNextmonth)
	ON_BN_CLICKED(IDC_ONE1, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_ONE2, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_ONE3, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_ONE4, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_ONE5, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_ONE6, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_ONE7, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_TWO1, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_TWO2, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_TWO3, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_TWO4, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_TWO5, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_TWO6, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_TWO7, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_THREE1, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_THREE2, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_THREE3, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_THREE4, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_THREE5, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_THREE6, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_THREE7, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FOUR1, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FOUR2, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FOUR3, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FOUR4, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FOUR5, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FOUR6, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FOUR7, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FIVE1, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FIVE2, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FIVE3, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FIVE4, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FIVE5, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FIVE6, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_FIVE7, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_SIX1, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_SIX2, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_SIX3, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_SIX4, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_SIX5, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_SIX6, &Cal01_View::OnDateClick)
	ON_BN_CLICKED(IDC_SIX7, &Cal01_View::OnDateClick)
	//create view to create new appointment
	ON_BN_CLICKED(IDC_BUTTON_ADDDATE, &Cal01_View::OnBnClickedButtonAdddate)
	//recieves messages
	ON_MESSAGE(WM_MYMESSAGE, OnMyMessage)
END_MESSAGE_MAP()

// Cal01_View-Meldungshandler

BOOL Cal01_View::OnInitDialog()
{
	//initialize dialog windows
	appointments->Create(AppointmentsView::IDD, this);
	appointments->SetWindowPos(NULL, 600, 20, 400, 600, SWP_NOZORDER);
	appointments->ShowWindow(SW_SHOW);

	//create database
	Database* database = Database::getInstance(); //nur Login
	AppointmentModel* model = new AppointmentModel(database); //Abstraktion von MFC

	//create view which holds appointments for a selected day
	AppointmentView* view = new AppointmentView(appointments);
	AppointmentController* controller = new AppointmentController(model, view);

	//register view to model and set controller
	model->registerObserver(view);
	this->setController(controller);

	//set month and year on dialog window
	monthAndYear.Format(cal->getMonthString(cal->getMonth()) + _T(" ") + _T("%d"), cal->getYear());
	UpdateData(FALSE);

	//debug
	_cprintf("painted\n");

	return TRUE;
}

//recieves messages from other dialogs and displays them
LRESULT Cal01_View::OnMyMessage(WPARAM wParam, LPARAM lParam)
{
	LPCTSTR lpszString = (LPCTSTR)lParam;
	outputLog.Format(lpszString);
	UpdateData(FALSE);
	return 0;
}

void Cal01_View::setController(AppointmentController* controller)
{
	this->controller = controller;
}

//get previous month
void Cal01_View::OnBnClickedButtonPreviousmonth()
{
	cal->previousMonth();
	showCal();
	updateText();	
}

//get next month
void Cal01_View::OnBnClickedButtonNextmonth()
{
	cal->nextMonth();
	showCal();
	updateText();
}

//updates text after switching month
void Cal01_View::updateText() {
	monthAndYear.Format(cal->getMonthString(cal->getMonth()) + _T(" ") + _T("%d"), cal->getYear());
	UpdateData(FALSE);
}

//initializes calendar
void Cal01_View::showCal() {
	int start = cal->getStartDay(cal->getMonth(), cal->getYear()); //which day is the first
	int days = cal->getDaysPerMonth(cal->getMonth());
	int counter = 1;
	//loop over buttons which represent a day
	for (int i = 1003; i < 1045; i++) {
		CWnd* pWnd = GetDlgItem(i);
		if (start == 1 && counter <= days) {
			pWnd->ShowWindow(TRUE);
			pWnd->SetWindowTextW(cal->getCalDaysAsString(counter));
			counter++;
		}
		else {
			start--;
			pWnd->ShowWindow(FALSE);
		}
	}
}

//click on a day
void Cal01_View::OnDateClick()
{
	appointments->ShowWindow(SW_SHOW);
	int year = cal->getYear();
	int month = cal->getMonth();
	int day = getDayFromButton();

	CTime date(year, month, day, 0, 0, 0);
	showDay(day, month, year);

	this->controller->showAppointmentsFromDate(date);
}

//get day from clicked button
int Cal01_View::getDayFromButton() {
	CString buttonTextDay;
	int d, buttonId;

	CWnd *pWnd = GetFocus(); //which Button was pressed
	if (pWnd != NULL) {
		buttonId = pWnd->GetDlgCtrlID();
	}
	pWnd = GetDlgItem(buttonId);

	pWnd->GetWindowTextW(buttonTextDay);
	d = _ttoi(buttonTextDay); //CString to Int
	return d;
}

//show selected day on dialog
void Cal01_View::showDay(int d, int m, int y) {
	cal->setDay(d);
	cal->setMonth(m);
	cal->setYear(y);
	buttonDate.Format(_T("%d.%d.%d"), d, m, y);
	UpdateData(FALSE);
}

//create new view to create new appointment
void Cal01_View::OnBnClickedButtonAdddate()
{
	int day = cal->getDay();
	int month = cal->getMonth();
	int year = cal->getYear();
	date_View->Create(Date_View2::IDD, this);
	date_View->ShowWindow(SW_SHOW);
}