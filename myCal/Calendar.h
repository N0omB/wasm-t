//Thomas
#pragma once


class Calendar
{
private:
	int day;
	int year;
	int month;

public:
	Calendar();
	~Calendar();
	//getter
	const int getDay();
	const int getMonth();
	const int getYear();
	//setter
	void setDay(int);
	void setMonth(int);
	void setYear(int);
	//methods
	void previousMonth();
	void nextMonth();
	CString getMonthString(int);
	CString getCalDaysAsString(int);
	int getMonthDigit(int);
	int getDaysPerMonth(int);
	bool isLeapYear(int);
	int getStartDay(int, int);
};