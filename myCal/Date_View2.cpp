//Thomas
// Date_View2.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "MyCal.h"
#include "Date_View2.h"
#include "afxdialogex.h"

// Date_View2-Dialogfeld

IMPLEMENT_DYNAMIC(Date_View2, CDialog)

Date_View2::Date_View2(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_DATE_VIEW2, pParent)
{
	//initialze datastructures for database queries
	this->addDate = new AddDate();
	this->database = Database::getInstance();
	this->roomModel = new RoomModel(this->database);
	this->roomEntities = this->roomModel->getRoomEntities();
	this->doctorModel = new DoctorModel(this->database);
	this->doctorEntities = this->doctorModel->getDoctorEntities();
	this->patientModel = new PatientModel(this->database);
	this->patientEntities = this->patientModel->getPatientEntities();
	this->appointmentModel = new AppointmentModel(this->database);
}

Date_View2::~Date_View2()
{
}

void Date_View2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//maps dialog elements to datastructures
	DDX_Control(pDX, IDC_COMBO1_DOCTOR, comboboxDoctor);
	DDX_Control(pDX, IDC_COMBO2_ROOM, comboboxRoom);
	DDX_Control(pDX, IDC_COMBO3_PATIENT, comboboxPatient);
	DDX_Control(pDX, IDC_COMBO4_START, comboboxStart);
	DDX_Control(pDX, IDC_COMBO5_DURATION, comboboxDuration);
	DDX_Control(pDX, IDC_CHECK1, checkMonday);
	DDX_Control(pDX, IDC_CHECK2, checkTuesday);
	DDX_Control(pDX, IDC_CHECK3, checkWednesday);
	DDX_Control(pDX, IDC_CHECK4, checkThursday);
	DDX_Control(pDX, IDC_CHECK5, checkFriday);
}

BEGIN_MESSAGE_MAP(Date_View2, CDialog)
	//maps buttons to functions
	ON_BN_CLICKED(IDOK, &Date_View2::OnOK)
	ON_BN_CLICKED(IDCANCEL, &Date_View2::OnCancel)
END_MESSAGE_MAP()

BOOL Date_View2::OnInitDialog()
{
	CDialog::OnInitDialog();
	//initialize comboboxes with data from database
	int i = 0; //Position in jeweiliger Combobox
	for (std::vector<DoctorEntity*>::iterator doctorIterator = this->doctorEntities.begin(); doctorIterator != this->doctorEntities.end(); ++doctorIterator) {
		CString name = (*doctorIterator)->getName();
		i = comboboxDoctor.AddString(name);
		comboboxDoctor.SetItemData(i, (*doctorIterator)->getId());
	}
	for (std::vector<RoomEntity*>::iterator roomIterator = this->roomEntities.begin(); roomIterator != this->roomEntities.end(); ++roomIterator) {
		CString description = (*roomIterator)->getDescription();
		i = comboboxRoom.AddString(description);
		comboboxRoom.SetItemData(i, (*roomIterator)->getId());
	}
	for (std::vector<PatientEntity*>::iterator patientIterator = this->patientEntities.begin(); patientIterator != this->patientEntities.end(); ++patientIterator) {
		CString name = (*patientIterator)->getName();
		i = comboboxPatient.AddString(name);
		comboboxPatient.SetItemData(i, (*patientIterator)->getId());
	}

	//initialize combobox with durations
	i = comboboxDuration.AddString(_T("5"));
	comboboxDuration.SetItemData(i, 5);
	i = comboboxDuration.AddString(_T("10"));
	comboboxDuration.SetItemData(i, 10);
	i = comboboxDuration.AddString(_T("15"));
	comboboxDuration.SetItemData(i, 15);
	i = comboboxDuration.AddString(_T("30"));
	comboboxDuration.SetItemData(i, 30);

	//initialize combobox with starttimes
	i = comboboxStart.AddString(_T("08:00"));
	comboboxStart.SetItemData(i, 800);
	i = comboboxStart.AddString(_T("09:00"));
	comboboxStart.SetItemData(i, 900);
	i = comboboxStart.AddString(_T("10:00"));
	comboboxStart.SetItemData(i, 1000);
	i = comboboxStart.AddString(_T("11:00"));
	comboboxStart.SetItemData(i, 1100);
	i = comboboxStart.AddString(_T("12:00"));
	comboboxStart.SetItemData(i, 1200);
	i = comboboxStart.AddString(_T("13:00"));
	comboboxStart.SetItemData(i, 1300);
	i = comboboxStart.AddString(_T("14:00"));
	comboboxStart.SetItemData(i, 1400);
	i = comboboxStart.AddString(_T("15:00"));
	comboboxStart.SetItemData(i, 1500);
	i = comboboxStart.AddString(_T("16:00"));
	comboboxStart.SetItemData(i, 1600);

	UpdateData(FALSE);
	return TRUE;
}

void Date_View2::OnOK()
{
	//Auswahl von Arzt, Patient, Doctor in Combobox
	int selectedDoctor, selectedPatient, selectedRoom;
	selectedDoctor = comboboxDoctor.GetCurSel();
	selectedDoctor = comboboxDoctor.GetItemData(selectedDoctor);

	selectedPatient = comboboxPatient.GetCurSel();
	selectedPatient = comboboxPatient.GetItemData(selectedPatient);

	selectedRoom = comboboxRoom.GetCurSel();
	selectedRoom = comboboxRoom.GetItemData(selectedRoom);

	//Auswahl der Tage
	bool days[7];
	days[5] = false; //Samstag
	days[6] = false; //Sonntag
	if (checkMonday.GetCheck() == 0) {
		days[0] = false;
	} else
	{
		days[0] = true;
	}
	if (checkTuesday.GetCheck() == 0) {
		days[1] = false;
	} else {
		days[1] = true;
	}
	if (checkWednesday.GetCheck() == 0) {
		days[2] = false;
	}
	else {
		days[2] = true;
	}
	if (checkThursday.GetCheck() == 0) {
		days[3] = false;
	}
	else {
		days[3] = true;
	}
	if (checkFriday.GetCheck() == 0) {
		days[4] = false;
	}
	else {
		days[4] = true;
	}

	//Auswahl von Zeitraum und Dauer
	int selectedDuration = comboboxDuration.GetCurSel();
	selectedDuration = comboboxDuration.GetItemData(selectedDuration);	

	int selectedStartTime = comboboxStart.GetCurSel();
	selectedStartTime = comboboxStart.GetItemData(selectedStartTime);

	//Termine ab aktuellen Datum holen
	//Bsp: CTime t1(int nYear, int nMonth, int nDay, int nHour, int nMin, int nSec, int nDST = -1);
	//Bsp: CTime t1(2016, 11, 16, 0, 0, 0, -1); //specific date
	CTime currentDate = CTime::GetCurrentTime();
	time_t currDate = currentDate.GetTime(); //time_t CTime::GetTime( ) const; => Returns a time_t value for the given CTime object.
	this->appointmentEntities = appointmentModel->getAppointmentsSinceDate(currentDate);


	//Umwandeln von Entities in Arrays f�r DLL
	//split appointments to arrays
	int *appointmentIds;
	int *doctorIds;
	int *roomIds;
	int *patientIds;
	time_t *appointmentDates;
	time_t *appointmentDurations;
	int *result; //will contain data for new appointment
	const int count = appointmentEntities.size(); //number of appointments
	
	//initialize arrays
	appointmentIds = new int[count];
	doctorIds = new int[count];
	roomIds = new int[count];
	patientIds = new int[count];
	appointmentDates = new time_t[count];
	appointmentDurations = new time_t[count];
	result = new int[21];

	//fill arrays with data
	for (int i = 0; i < appointmentEntities.size(); i++) {
		appointmentIds[i] = appointmentEntities[i]->getId();
		doctorIds[i] = appointmentEntities[i]->getDoctorEntity()->getId();
		roomIds[i] = appointmentEntities[i]->getRoomEntity()->getId();
		patientIds[i] = appointmentEntities[i]->getPatientEntity()->getId();
		appointmentDates[i] = appointmentEntities[i]->getDateTime().GetTime();
		appointmentDurations[i] = appointmentEntities[i]->getDuration().GetTime();
	}

	if (selectedRoom == -1) {
		selectedRoom = 0;
	}

	//Test ob alle Infos ausgew�hlt wurden -> sonst keine Suche
	if (selectedDoctor != -1 && selectedPatient != -1 && selectedDuration != -1 && selectedStartTime != -1 && (days[0] != false || days[1] != false || days[2] != false || days[3] != false || days[4] != false)) {
		//definition of function from dll
		typedef void(*GETAPPOINTMENTS) (int, int, int, time_t, int, int, bool[], int*, int*, int*, int*, time_t*, time_t*, int, int*);

		//call dll
		HMODULE dll = LoadLibrary(L"AppointmentDll.dll");
		//is dll available?
		if (dll != NULL) {
			GETAPPOINTMENTS func = (GETAPPOINTMENTS)GetProcAddress(dll, "getAppointments");
			//is function available?
			if (func != NULL) {
				//call function
				func(selectedDoctor, selectedRoom, selectedPatient, currDate, selectedStartTime, selectedDuration, days, appointmentIds, doctorIds, roomIds, patientIds, appointmentDates, appointmentDurations, count, result);
				//debug
				_cprintf("\n");
				_cprintf("--------MyCal-------\n");
				_cprintf("Tag: %d.%d.%d\n", result[5], result[6], result[7]);
				_cprintf("Uhrzeit: %d:%d\n", result[4] / 100, result[4] % 100);

				//create new appointment in database
				this->database->insertAppointment(result);

				//display appointment in main window
				int min_tmp = result[4] % 100;
				int hour_tmp = result[4] / 100;
				CString message;
				if (min_tmp < 10) {
					message.Format(_T("Termin gefunden: %d.%d.%d -- %d:0%d"), result[5], result[6], result[7], hour_tmp, min_tmp);
				}
				else {
					message.Format(_T("Termin gefunden: %d.%d.%d -- %d:%d"), result[5], result[6], result[7], hour_tmp, min_tmp);
				}
				//send message to main window
				GetParent()->SendMessage(WM_MYMESSAGE, 0, (LPARAM)((LPCTSTR)message));
			}
			else {
				_cprintf("I can't load _getAppointments function!");
			}
		}
		else {
			_cprintf("I can't load this dll!");
		}
		FreeLibrary(dll);
		AFX_MANAGE_STATE(AfxGetStaticModuleState());

		//delete arrays
		delete[] appointmentIds;
		delete[] doctorIds;
		delete[] roomIds;
		delete[] patientIds;
		delete[] appointmentDates;
		delete[] appointmentDuration;
		delete[] result;
	}

	//debug
	bool debug = false;
	if (debug) {
		_cprintf("--------------------\n");
		_cprintf("ArztId: %d\n", selectedDoctor);
		_cprintf("RaumId: %d\n", selectedRoom);
		_cprintf("PatientId: %d\n", selectedPatient);

		for (int i = 0; i < 7; i++) {
			if (days[i]) {
				_cprintf("Tage: %d: Yes!\n", i);
			}
			else {
				_cprintf("Tage: %d: No!\n", i);
			}
		}

		_cprintf("Duration: %d\n", selectedDuration);
		_cprintf("StartTime: %d\n", selectedStartTime);

		for (int i = 0; i < appointmentEntities.size(); i++) {
			CString str;
			CTime t = appointmentEntities[i]->getDateTime();
			time_t datum = t.GetTime();
			tm* tmZeit = localtime(&datum);
			_cprintf("TerminID: %d -- ", appointmentEntities[i]->getId());
			str = appointmentEntities[i]->getDoctorEntity()->getName();
			_cprintf("Arzt: %ls(%d) -- ", str, appointmentEntities[i]->getDoctorEntity()->getId());
			str = appointmentEntities[i]->getPatientEntity()->getName();
			_cprintf("Patient: %ls(%d) -- ", str, appointmentEntities[i]->getPatientEntity()->getId());
			str = appointmentEntities[i]->getRoomEntity()->getDescription();
			_cprintf("Raum: %ls(%d)\n", str, appointmentEntities[i]->getRoomEntity()->getId());
			_cprintf("Datum: %d.%d.%d\n", tmZeit->tm_mday, tmZeit->tm_mon + 1, tmZeit->tm_year + 1900);
			_cprintf("---\n");
		}

		_cprintf("--------------------\n");
	}

	UpdateData(FALSE);
}


void Date_View2::OnCancel()
{
	//send message to main windows
	GetParent()->SendMessage(WM_MYMESSAGE, 0, (LPARAM)((LPCTSTR)_T("AddDate-Window was closed")));
	this->DestroyWindow();
}