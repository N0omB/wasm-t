//Tobias
#include "Calendar.h"


Calendar::Calendar()
{
	//get current date
	day = CTime::GetCurrentTime().GetDay();
	month = CTime::GetCurrentTime().GetMonth();
	year = CTime::GetCurrentTime().GetYear();
}


Calendar::~Calendar()
{
}

//getter
const int Calendar::getDay() { return day; }
const int Calendar::getMonth() { return month; }
const int Calendar::getYear() { return year; }

//setter
void Calendar::setDay(int d) { day = d;  }
void Calendar::setMonth(int m) { month = m; }
void Calendar::setYear(int y) { year = y; }

//methods
//get previous month
void Calendar::previousMonth() {
	if (month != 1) {
		month--;
	}
	else {
		month = 12;
		year--;
	}
}

//get next month
void Calendar::nextMonth() {
	if (month != 12) {
		month++;
	}
	else {
		month = 1;
		year++;
	}
}

//get month name
CString Calendar::getMonthString(int m) {
	switch (m) {
	case 1:
		return _T("Jan");
		break;
	case 2:
		return _T("Feb");
		break;
	case 3:
		return _T("Mar");
		break;
	case 4:
		return _T("Apr");
		break;
	case 5:
		return _T("May");
		break;
	case 6:
		return _T("Jun");
		break;
	case 7:
		return _T("Jul");
		break;
	case 8:
		return _T("Aug");
		break;
	case 9:
		return _T("Sep");
		break;
	case 10:
		return _T("Oct");
		break;
	case 11:
		return _T("Nov");
		break;
	case 12:
		return _T("Dec");
		break;
	default:
		return _T("...");
		break;
	}
}

//get day as string
CString Calendar::getCalDaysAsString(int d) {
	switch (d) {
	case 1:
		return _T("1");
		break;
	case 2:
		return _T("2");
		break;
	case 3:
		return _T("3");
		break;
	case 4:
		return _T("4");
		break;
	case 5:
		return _T("5");
		break;
	case 6:
		return _T("6");
		break;
	case 7:
		return _T("7");
		break;
	case 8:
		return _T("8");
		break;
	case 9:
		return _T("9");
		break;
	case 10:
		return _T("10");
		break;
	case 11:
		return _T("11");
		break;
	case 12:
		return _T("12");
		break;
	case 13:
		return _T("13");
		break;
	case 14:
		return _T("14");
		break;
	case 15:
		return _T("15");
		break;
	case 16:
		return _T("16");
		break;
	case 17:
		return _T("17");
		break;
	case 18:
		return _T("18");
		break;
	case 19:
		return _T("19");
		break;
	case 20:
		return _T("20");
		break;
	case 21:
		return _T("21");
		break;
	case 22:
		return _T("22");
		break;
	case 23:
		return _T("23");
		break;
	case 24:
		return _T("24");
		break;
	case 25:
		return _T("25");
		break;
	case 26:
		return _T("26");
		break;
	case 27:
		return _T("27");
		break;
	case 28:
		return _T("28");
		break;
	case 29:
		return _T("29");
		break;
	case 30:
		return _T("30");
		break;
	case 31:
		return _T("31");
		break;
	default:
		return _T("...");
		break;
	}
}

//get month digit
int Calendar::getMonthDigit(int m) {
	switch (m) {
	case 1:
		return 0;
		break;
	case 2:
		return 3;
		break;
	case 3:
		return 3;
		break;
	case 4:
		return 6;
		break;
	case 5:
		return 1;
		break;
	case 6:
		return 4;
		break;
	case 7:
		return 6;
		break;
	case 8:
		return 2;
		break;
	case 9:
		return 5;
		break;
	case 10:
		return 0;
		break;
	case 11:
		return 3;
		break;
	case 12:
		return 5;
		break;
	default:
		return -1;
		break;
	}
}

//get days per month
int Calendar::getDaysPerMonth(int m)
{
	switch (m) {
	case 1:
		return 31;
		break;
	case 2:
		if (isLeapYear(year)) {
			return 29;
		}
		else {
			return 28;
		}
		break;
	case 3:
		return 31;
		break;
	case 4:
		return 30;
		break;
	case 5:
		return 31;
		break;
	case 6:
		return 30;
		break;
	case 7:
		return 31;
		break;
	case 8:
		return 31;
		break;
	case 9:
		return 30;
		break;
	case 10:
		return 31;
		break;
	case 11:
		return 30;
		break;
	case 12:
		return 31;
		break;
	default:
		return -1;
		break;
	}
}

//is year a leap year?
bool Calendar::isLeapYear(int y) {
	bool leap_year;

	if (y % 400 == 0) {
		leap_year = true;
	}
	else if (y % 100 == 0) {
		leap_year = false;
	}
	else if (y % 4 == 0) {
		leap_year = true;
	}
	else {
		leap_year = false;
	}

	if (leap_year) {
		return true;
	}
	else {
		return false;
	}
}

//returns Start Day in Month
int Calendar::getStartDay(int m, int y) {
	int n_day, n_month, n_year_ahead, n_year_behind, n_leap;

	int tmp_day = 1;
	int tmp_month = m;
	int tmp_year = y;

	int year_first_digits = tmp_year / 100;
	int year_last_digits = tmp_year % 100;

	n_day = tmp_day % 7; //immer erster Tag im Monat
	n_month = getMonthDigit(m); //f�r jedes Jahr gleich
	n_year_behind = (year_last_digits + (year_last_digits / 4)) % 7;
	n_year_ahead = (3 - (year_first_digits % 4)) * 2;
	if (isLeapYear(y) && (tmp_month == 1 || tmp_month == 2)) {
		n_leap = 6;
	}
	else {
		n_leap = 0;
	}

	int final = (n_day + n_month + n_year_ahead + n_year_behind + n_leap) % 7;
	if (final == 0) {
		final = 7;
	}
	return final;
}