//Thomas
#pragma once

#include "Calendar.h"
#include "AppointmentController.h"
#include "AppointmentsView.h"

// Cal01_View-Dialogfeld
#define WM_MYMESSAGE	WM_APP+1 //defines command for messaging system
class Cal01_View : public CDialog
{
	DECLARE_DYNAMIC(Cal01_View)

private:
	Calendar *cal;
	Date_View2 *date_View;
	AppointmentsView* appointments;
	AppointmentController* controller;
	CString monthAndYear;
	CString buttonDate;
	CString outputLog;
	virtual BOOL OnInitDialog();

public:
	Cal01_View(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~Cal01_View();
	void setController(AppointmentController* controller);
	void updateText();
	void showCal();
	int getDayFromButton();
	void showDay(int, int, int);
	afx_msg void OnBnClickedButtonPreviousmonth();
	afx_msg void OnBnClickedButtonNextmonth();
	afx_msg void OnDateClick();
	afx_msg void OnBnClickedButtonAdddate();
	//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CAL01_DIALOG };
	//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	afx_msg LRESULT OnMyMessage(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};
