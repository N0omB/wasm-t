//Thomas
#pragma once

#include "afxwin.h"
#include <vector>
#include "AddDate.h"
#include "AppointmentModel.h"
#include "Database.h"
#include "DoctorModel.h"
#include "PatientModel.h"
#include "RoomModel.h"

// Date_View2-Dialogfeld
#define WM_MYMESSAGE	WM_APP+1 //defines command for messaging system
class Date_View2 : public CDialog
{
	DECLARE_DYNAMIC(Date_View2)

public:
	Date_View2(CWnd* pParent = NULL);
	virtual ~Date_View2();
	enum { IDD = IDD_DATE_VIEW2 };
	CComboBox comboboxStart;
	CComboBox comboboxEnd;
	CComboBox comboboxDuration;
	CComboBox comboboxDoctor;
	CComboBox comboboxRoom;
	CComboBox comboboxPatient;
	CEdit appointmentDuration;
	CButton checkMonday;
	CButton checkTuesday;
	CButton checkWednesday;
	CButton checkThursday;
	CButton checkFriday;
	afx_msg void OnOK();
	afx_msg void OnCancel();

private:
	AddDate* addDate;
	DoctorModel* doctorModel;
	PatientModel* patientModel;
	RoomModel* roomModel;
	AppointmentModel* appointmentModel;
	Database* database;
	std::vector<DoctorEntity*> doctorEntities;
	std::vector<PatientEntity*> patientEntities;
	std::vector<RoomEntity*> roomEntities;
	std::vector<AppointmentEntity*> appointmentEntities;
	virtual BOOL OnInitDialog();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	DECLARE_MESSAGE_MAP()
};
