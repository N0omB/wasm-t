import clang.cindex, os.path
from clang.cindex import CursorKind_FUNCTION_DECL, CursorKind_CXX_METHOD, CursorKind_TRANSLATION_UNIT, \
    CursorKind_INCLUSION_DIRECTIVE, CursorKind_PREPROCESSING_DIRECTIVE, CursorKind_CLASS_DECL, CursorKind_CONSTRUCTOR, \
    CursorKind_DESTRUCTOR, CursorKind_FIELD_DECL, CursorKind_VAR_DECL, CursorKind_PARM_DECL, CursorKind_CALL_EXPR, \
    CursorKind_DECL_REF_EXPR, CursorKind_MEMBER_REF_EXPR

'''
##############################################################################################################
                                        Test Functions with prints
##############################################################################################################
'''


def find_function_by_name(node, function_name):
    ref_node = node.get_definition()
    if node.kind == CursorKind_FUNCTION_DECL and hasattr(ref_node, 'spelling') and ref_node.spelling == function_name:
        print('Found %s [line=%s, col=%s]' % (
            node.spelling, node.location.line, node.location.column))
    for c in node.get_children():
        find_function_by_name(c, function_name)


def get_function_body_indexes(node, function_name):
    for f in node.walk_preorder():
        ref_node = f.get_definition()
        if f.kind == CursorKind_FUNCTION_DECL and hasattr(ref_node, 'spelling') and ref_node.spelling == function_name:
            print(f.extent)


def find_typerefs(node, typename):
    """ Find all references to the type named 'typename'
    """
    if node.kind.is_reference():
        ref_node = node.get_definition()
        if hasattr(ref_node, 'spelling') and ref_node.spelling == typename:
            print('Found %s [line=%s, col=%s]' % (
                typename, node.location.line, node.location.column))
    # Recurse for children of this node
    for c in node.get_children():
        find_typerefs(c, typename)


'''
##############################################################################################################
                                            API classes
##############################################################################################################
'''


class Variable(object):
    def __init__(self, node, name=None, type=None, location=None):
        if node is not None:
            self.node = node
            self.name = node.spelling
            self.type = node.type.spelling
            self.line = node.location.line
            self.location = os.path.basename(node.location.file.name)
        elif name is not None and type is not None and location is not None:
            self.name = name
            self.type = type
            self.location = location

    def __str__(self):
        return "(Name: " + self.name + ", Type: " + self.type + ")"

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        if isinstance(self, other.__class__):
            return self.location == other.location and self.name == other.name and self.type == other.type
        return NotImplemented

    def is_var_decl(self):
        return self.node.kind == CursorKind_VAR_DECL

    def is_field_decl(self):
        return self.node.kind == CursorKind_FIELD_DECL


class Function(object):
    def cxx_full_name(self, n):
        if n is None or n.kind == CursorKind_TRANSLATION_UNIT:
            return ''
        else:
            res = self.cxx_full_name(n.semantic_parent)
            if res != '':
                return res + '::' + n.spelling
        return n.spelling

    def is_constructor(self):
        return self.kind == CursorKind_CONSTRUCTOR

    def add_parameter(self, name, type, location):
        self.parameters.append(Variable(None, name, type, location))

    def __init__(self, node, found_in_class_decl=False):
        self.node = node
        self.location = os.path.basename(node.location.file.name)
        self.name = node.spelling
        self.spelling = node.spelling
        self.start = node.extent.start.line
        self.end = node.extent.end.line
        self.type = node.type.get_result().spelling
        self.kind = node.kind

        self.parameters = []
        for n in list(node.get_arguments()):
            self.parameters.append(Variable(n))

        if node.kind == CursorKind_CXX_METHOD and not found_in_class_decl:
            self.name = self.cxx_full_name(node)

    def __str__(self):
        return "(Name: " + self.name + ", Return: " + self.type + ", Arguments: " + self.parameters.__repr__() + ")"

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        if isinstance(self, other.__class__):
            return self.location == other.location and self.name == other.name and self.type == other.type and \
                   len(self.parameters) == len(self.parameters) and \
                   all(s == o for s, o in zip(self.parameters, other.parameters))
        return NotImplemented


class Including(object):
    def __init__(self, node):
        self.node = node
        self.name = node.spelling
        self.location = os.path.basename(node.location.file.name)
        self.line = node.location.line

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)


class Class(object):
    def __init__(self, node):
        self.node = node
        self.location = os.path.basename(node.location.file.name)
        self.name = node.displayname
        self.start = node.extent.start.line
        self.end = node.extent.end.line

        self.variables = []
        self.functions = []
        for n in node.walk_preorder():
            if n.kind == clang.cindex.CursorKind_FIELD_DECL:
                self.variables.append(Variable(n))
            elif n.kind == CursorKind_CXX_METHOD or n.kind == CursorKind_CONSTRUCTOR or n.kind == CursorKind_DESTRUCTOR:
                self.functions.append(Function(n, True))

    def __str__(self):
        return "(Name: " + self.name + ", Variables: " + self.variables.__repr__() + ", Functions: " \
               + self.functions.__repr__() + ")"

    def __repr__(self):
        return str(self)


class CallExpression(object):
    def __init__(self, node):
        self.node = node
        self.location = os.path.basename(node.location.file.name)
        self.line = node.location.line
        self.name = node.displayname
        self.definition = node.get_definition()

    def __str__(self):
        return "(Name: " + self.name + ", Location: " + self.location + ", Line: " + self.line + ")"

    def __repr__(self):
        return str(self)

'''
##############################################################################################################
                                            API functions
##############################################################################################################
'''


def get_main_cursor(file_path, pre=False):
    index = clang.cindex.Index.create()
    if pre:
        translation_unit = index.parse(file_path, args=['-x', 'c++', '--std=c++11'], options=clang.cindex.TranslationUnit.PARSE_DETAILED_PROCESSING_RECORD)
    else:
        translation_unit = index.parse(file_path, args=['-x', 'c++', '--std=c++11'])
    return translation_unit.cursor


def get_related_files_in_dir(cursor, name_list):
    related_files = set()
    for n in cursor.walk_preorder():
        if hasattr(n, "location") and hasattr(n.location, "file") and hasattr(n.location.file, "name") \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname)\
                and not any(os.path.splitext(os.path.basename(n.location.file.name))[0] in name for name in name_list):
            related_files.update([os.path.basename(n.location.file.name)])
    return related_files, list(related_files)


def get_function_start_end(cursor, function_name, start_index=1):
    for n in cursor.walk_preorder():
        if n.kind == CursorKind_FUNCTION_DECL and hasattr(n, 'spelling') and n.spelling == function_name:
            if start_index == 1:
                return n.extent.start.line, n.extent.end.line
            else:
                diff = start_index - 1
                return n.extent.start.line + diff, n.extent.end.line + diff
    return -1, -1


def get_function_start_end_in_main_file(cursor, function_name, start_index=1):
    for n in cursor.walk_preorder():
        if (n.kind == CursorKind_FUNCTION_DECL or n.kind == CursorKind_CXX_METHOD or n.kind == CursorKind_CONSTRUCTOR
            or n.kind == CursorKind_DESTRUCTOR) and hasattr(n, 'spelling') and n.spelling == function_name \
                and os.path.basename(n.location.file.name) == os.path.basename(cursor.displayname):
            if start_index == 1:
                return n.extent.start.line, n.extent.end.line
            else:
                diff = start_index - 1
                return n.extent.start.line + diff, n.extent.end.line + diff
    return -1, -1


def get_functions_start_end(cursor, function_names, start_index=1):
    return_list = []
    for n in cursor.walk_preorder():
        if n.kind == CursorKind_FUNCTION_DECL and hasattr(n, 'spelling') and any(
                n.spelling == name for name in function_names):
            if start_index == 1:
                return_list.append((n.spelling, n.extent.start.line, n.extent.end.line))
            else:
                diff = start_index - 1
                return_list.append((n.spelling, n.extent.start.line + diff, n.extent.end.line + diff))
    return return_list


def get_function_definition_by_name_in_mainFile(cursor, name): # main_file is the only read for this func
    for n in cursor.walk_preorder():
        if (n.kind == CursorKind_FUNCTION_DECL or n.kind == CursorKind_CXX_METHOD or n.kind == CursorKind_CONSTRUCTOR
            or n.kind == CursorKind_DESTRUCTOR) and n.is_definition() and n.spelling == name \
                and os.path.basename(n.location.file.name) == os.path.basename(cursor.displayname):
            return Function(n)
    return None


def get_function_definitions(cursor):
    return_list = []
    for n in cursor.walk_preorder():
        if (n.kind == CursorKind_FUNCTION_DECL or n.kind == CursorKind_CXX_METHOD or n.kind == CursorKind_CONSTRUCTOR
            or n.kind == CursorKind_DESTRUCTOR) and n.is_definition() \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname):
            return_list.append(Function(n))
    return return_list


def get_function_definitions_at_file(cursor, file_name):
    functions = []
    for n in cursor.walk_preorder():
        if (n.kind == CursorKind_FUNCTION_DECL or n.kind == CursorKind_CXX_METHOD or n.kind == CursorKind_CONSTRUCTOR
            or n.kind == CursorKind_DESTRUCTOR) and n.is_definition() \
                and os.path.basename(n.location.file.name) == file_name:
            functions.append(Function(n))
    return functions


def get_function_definition_by_obj(cursor, func_obj):
    for n in cursor.walk_preorder():
        if (n.kind == CursorKind_FUNCTION_DECL or n.kind == CursorKind_CXX_METHOD or n.kind == CursorKind_CONSTRUCTOR
            or n.kind == CursorKind_DESTRUCTOR) and n.is_definition() and n.spelling == func_obj.spelling \
                and os.path.basename(n.location.file.name) == func_obj.location:
            func = Function(n)
            if func == func_obj:
                return func
    return None


def get_class_definitions(cursor):
    return_list = []
    for n in cursor.walk_preorder():
        if n.kind == CursorKind_CLASS_DECL \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname):
            return_list.append(Class(n))
    return return_list


def get_class_definition_by_name(cursor, name):
    for n in cursor.walk_preorder():
        if n.kind == CursorKind_CLASS_DECL and n.spelling == name \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname):
            return Class(n)
    return None


def get_param_locations(cursor, param):
    params = []
    function_name = param.node.semantic_parent.spelling
    function_parent_name = param.node.semantic_parent.semantic_parent.spelling
    for n in cursor.walk_preorder():
        if n.kind == CursorKind_PARM_DECL and function_name == n.semantic_parent.spelling \
                and function_parent_name == n.semantic_parent.semantic_parent.spelling:
            params.append(Variable(n))
    return params


def get_params(cursor, only_def=False):
    definition = True
    return_list = []
    for n in cursor.walk_preorder():
        if only_def and hasattr(n, "lexical_parent") and n.lexical_parent is not None:
            definition = n.lexical_parent.is_definition()
        if n.kind == CursorKind_PARM_DECL and definition \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname):
            return_list.append(Variable(n))
    return return_list


def get_variables_and_fields(cursor):
    return_list = []
    for n in cursor.walk_preorder():
        if (n.kind == CursorKind_FIELD_DECL or n.kind == CursorKind_VAR_DECL) \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname):
            return_list.append(Variable(n))
    return return_list


def get_include_directives(cursor, files_set):
    includes = []
    for n in cursor.get_children():
        if n.kind == CursorKind_INCLUSION_DIRECTIVE and n.location.file is not None \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname)\
                and os.path.basename(n.location.file.name) in files_set:
            includes.append(Including(n))
    return includes


def get_preprocessor_directives(cursor):
    preprocessor_directives = []
    for n in cursor.get_children():
        if n.kind == CursorKind_PREPROCESSING_DIRECTIVE and n.location.file is not None \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname):
            preprocessor_directives.append(n)
    return preprocessor_directives


def get_var_usages(cursor):
    var_usages = []
    for n in cursor.walk_preorder():
        if (n.kind == CursorKind_DECL_REF_EXPR or n.kind == CursorKind_MEMBER_REF_EXPR) and n.location.file is not None \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname):
            var_usages.append(n)
    return var_usages


def get_var_usages_of_defn(cursor, defn):
    var_usages = []
    for n in cursor.walk_preorder():
        if n.kind == CursorKind_DECL_REF_EXPR and n.location.file is not None \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname) \
                and n.get_definition == defn:
            var_usages.append(n)
    return var_usages


def get_field_usages_of_defn(cursor, defn):
    field_usages = []
    for n in cursor.walk_preorder():
        if n.kind == CursorKind_MEMBER_REF_EXPR and n.location.file is not None \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname) \
                and n.get_definition == defn:
            field_usages.append(n)
    return field_usages


def get_call_expr(cursor):
    call_expr = []
    for n in cursor.walk_preorder():
        if n.kind == CursorKind_CALL_EXPR and n.location.file is not None \
                and os.path.dirname(n.location.file.name) == os.path.dirname(cursor.displayname):
            call_expr.append(CallExpression(n))
    return call_expr


if __name__ == '__main__':
    # tutorial for first steps at: https://eli.thegreenplace.net/2011/07/03/parsing-c-in-python-with-clang
    file_path = "temp/hello.cpp"
    main_cursor = get_main_cursor(file_path)
    print('Translation unit:', main_cursor.displayname)
    # find_typerefs(main_cursor, "Box")
    # find_function_by_name(main_cursor, "doubleBox")

    # get_function_body_indexes(main_cursor, "doubleBox")
    # print(get_function_start_end(main_cursor, "doubleBox"))
    # print(get_functions_start_end(main_cursor, ["doubleBox", "add200", "add300"], 0))

    funcs = get_function_definitions(main_cursor)
    print(funcs)
    print(" ")
    # unknown_funcs = get_unknown_function(main_cursor, [f.name for f in funcs])
    # print(unknown_funcs)
    # print(" ")
    includes = get_include_directives(main_cursor)
    print(includes)


